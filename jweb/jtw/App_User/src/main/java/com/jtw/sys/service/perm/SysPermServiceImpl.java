package com.jtw.sys.service.perm;

import com.github.pagehelper.Page;
import com.jtw.common.bean.enums.ReqCode;
import com.jtw.common.bean.vo.FindPagingVo;
import com.jtw.common.bean.vo.PagingVo;
import com.jtw.common.util.PagingUtil;
import com.jtw.conf.shiro.util.ShiroUtil;
import com.jtw.sys.mapper.perm.SysPermMapper;
import com.jtw.sys.mapper.user.SysRoleMapper;
import com.jtw.sys.model.perm.TSysPerm;
import com.jtw.sys.vo.perm.SysPermTreeVo;
import com.jtw.sys.vo.perm.SysRolePermAddVo;
import org.apache.shiro.spring.web.ShiroFilterFactoryBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * DESCRIPT:
 *
 * @author cjsky666
 * @date 2018/9/12 13:56
 */
@Service
public class SysPermServiceImpl implements SysPermService {
    @Autowired
    private SysPermMapper sysPermMapper;
    @Autowired
    private SysRoleMapper sysRoleMapper;
    @Autowired
    private ShiroFilterFactoryBean shiroFilterFactoryBean;
    @Autowired
    private AuthService authService;

    @Override
    public void save(TSysPerm tSysPerm) {
        sysPermMapper.insertSelective(tSysPerm);

    }

    @Override
    public void saveAll(List<TSysPerm> list) {
        sysPermMapper.insertList(list);
    }

    @Override
    public void update(TSysPerm tSysPerm) {
        sysPermMapper.updateByPrimaryKeySelective(tSysPerm);
    }

    @Override
    public void delete(Object id) {
        sysPermMapper.deleteByPrimaryKey(id);
    }



    @Override
    public TSysPerm getById(Object id) {
        return sysPermMapper.selectByPrimaryKey(id);
    }

    @Override
    public Page<TSysPerm> findAll(PagingVo pagingVo) {
        PagingUtil.start(pagingVo);
        pagingVo.setOrderBy("ORDER BY `GROUP` DESC");
        return sysPermMapper.findAll();
    }

    @Override
    public Page<TSysPerm> findAll(FindPagingVo findPagingVo) {
        return null;
    }


    @Override
    public List<TSysPerm> getAll() {
        return null;
    }

    public List<String> getAllPermUrl(){
        return sysPermMapper.getAllPermUrl();
    }

    @Transactional
    public void modifyRolePerms(Integer roleId, Integer[] perms) {
        if(!sysRoleMapper.isSysRoleExisting(roleId)){
            throw ReqCode.UnExistingSysRole.getException();
        }
        for(Integer id: perms){
            if(!sysPermMapper.isSysPermExisting(id)){
                throw ReqCode.UnExistingSysRequest.getException("ID为"+id + "的权限不存在");
            }
        }
        sysPermMapper.deleteSysRolePermByRoleId(roleId);
        sysPermMapper.modifyRolePerms(roleId,perms);
        try {
            ShiroUtil.updateFilterChain(shiroFilterFactoryBean, authService);
        } catch (Exception e) {
            e.printStackTrace();
            throw ReqCode.SetSysRoleRequestFailed.getException();
        }
    }

    @Override
    public List<TSysPerm> getSysRolePerms(Integer roleId) {
        return null;
    }

    @Override
    public List<SysRolePermAddVo> getAllRolePermsByRoleIdNotHave(Integer roleId) {
        return sysPermMapper.getAllRolePermsByRoleIdNotHave(roleId);
    }

    @Override
    public List<SysRolePermAddVo> getSysUserRolePermsByUserId(Integer userId) {
        return null;
    }

    @Override
    public List<SysRolePermAddVo> getAllRolePermsByRoleId(Integer roleId) {
        return sysPermMapper.getAllRolePermsByRoleId(roleId);
    }

    @Override
    public void updateSysPerm(TSysPerm sysPerm) {
        if(sysPermMapper.updateByPrimaryKeySelective(sysPerm) <= 0){
            throw ReqCode.UnExistingSysRequest.getException();
        }
        try {
            ShiroUtil.updateFilterChain(shiroFilterFactoryBean, authService);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<SysPermTreeVo> getAllSysPermTreeMarkByRoleId(Integer roleId) {
        if(!sysRoleMapper.isSysRoleExisting(roleId)){
            throw ReqCode.UnExistingSysRole.getException();
        }
        return sysPermMapper.getAllSysPermTreeMarkByRoleId(roleId);
    }
}

package com.jtw.sys.service.log;

import com.github.pagehelper.Page;
import com.jtw.common.baseService.BaseService;
import com.jtw.common.bean.vo.FindPagingVo;
import com.jtw.common.bean.vo.PagingVo;
import com.jtw.common.util.PagingUtil;
import com.jtw.sys.mapper.log.SysAccessMapper;
import com.jtw.sys.model.log.TSysAccessLog;
import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * DESCRIPT: 系统用户登录记录表
 *
 * @author cjsky666
 * @date 2018/9/21 13:08
 */
@Service
public class SysAccessServiceImpl implements BaseService<TSysAccessLog> {
    @Autowired
    private SysAccessMapper sysAccessMapper;

    @Override
    public void save(TSysAccessLog tSysAccessLog) {
        sysAccessMapper.saveBackId(tSysAccessLog);
    }

    @Override
    public void saveAll(List<TSysAccessLog> list) {
        sysAccessMapper.insertList(list);
    }

    @Override
    public void update(TSysAccessLog tSysAccessLog) {

    }

    @Override
    public void delete(Object id) {

    }

    @Override
    public TSysAccessLog getById(Object id) {
        return null;
    }

    @Override
    public Page<TSysAccessLog> findAll(PagingVo pagingVo) {
        return null;
    }

    @Override
    public Page<TSysAccessLog> findAll(FindPagingVo findPagingVo) {
        PagingUtil.start(findPagingVo);
        return sysAccessMapper.finAll(PagingUtil.buildWhereSQLToString(findPagingVo.getFindParams()));
    }

    @Override
    public List<TSysAccessLog> getAll() {
        return null;
    }

    public void delBeforeDay(Integer day) {
        LocalDate now = LocalDate.now();
        sysAccessMapper.delBeforeDay(now.minusDays(day).toDate());
    }
}

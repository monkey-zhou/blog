package com.jtw.sys.service.perm;

import com.jtw.conf.shiro.model.BaseRole;
import com.jtw.conf.shiro.model.BaseUser;
import com.jtw.conf.shiro.model.FilterPerm;
import com.jtw.conf.shiro.model.FilterPermRole;
import com.jtw.conf.shiro.service.BasePermService;
import com.jtw.sys.mapper.perm.PermMapper;
import com.jtw.sys.model.perm.TSysPerm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * DESCRIPT:shiro权限校验的实现类-------系统级别
 *
 * @author cjsky666
 * @date 2018/9/3 15:33
 */
@Service
public class AuthService implements BasePermService {

    @Autowired
    PermMapper permMapper;

    @Override
    public Set<BaseRole> getRolesByUserName(String userName) {
        return permMapper.getRolesByUserName(userName);
    }

    @Override
    public BaseUser getUserByUserName(String userName) {
        return permMapper.getUserByUserName(userName);
    }

    @Override
    public List<FilterPerm> getAllPerm() {
        Map<String, FilterPerm> filterRequestMap = new HashMap<>();
        List<TSysPerm> tSysPerms = permMapper.getAllPerm();
        for (TSysPerm tSysPerm : tSysPerms) {
            FilterPerm fp;
            if (filterRequestMap.containsKey(tSysPerm.getUrl())) {
                fp = filterRequestMap.get(tSysPerm.getUrl());
                fp.setState(tSysPerm.getState());
                if (fp == null) continue;
                List<FilterPermRole> roles = fp.getFilterPermRoleList();
                if (roles == null) continue;
                FilterPermRole role = new FilterPermRole();
                role.setHttpMethod(tSysPerm.getHttpMethod());
                List<BaseRole> roleList = permMapper.getRolesByPermId(tSysPerm.getId());
                role.setBaseRoleList(roleList);
                roles.add(role);
            } else {
                fp = new FilterPerm();
                fp.setState(tSysPerm.getState());
                fp.setUrl(tSysPerm.getUrl());
                fp.setOpen(tSysPerm.getOpen());
                List<FilterPermRole> roles = new ArrayList<>();
                FilterPermRole role = new FilterPermRole();
                role.setHttpMethod(tSysPerm.getHttpMethod());
                List<BaseRole> roleList = permMapper.getRolesByPermId(tSysPerm.getId());
                role.setBaseRoleList(roleList);
                roles.add(role);
                fp.setFilterPermRoleList(roles);
                filterRequestMap.put(tSysPerm.getUrl(),fp);
            }
        }
        return new ArrayList<>(filterRequestMap.values());
    }

    @Override
    public BaseUser getUserByMobile(String mobile) {
        return permMapper.getUserByMobile(mobile);
    }

    @Override
    public BaseUser getUserByWXAPP(String wxAppOpenId) {
        return permMapper.getUserByWXAPP(wxAppOpenId);
    }

    @Override
    public BaseUser getUserByWXGZH(String wxGzhOpenId) {
        return permMapper.getUserByWXGZH(wxGzhOpenId);
    }

    @Override
    public BaseUser getUserByTXUNION(String txUnionId) {
        return permMapper.getUserByTXUNION( txUnionId);
    }

    @Override
    public BaseUser getUserByZFB(String zfbOpenId) {
        return permMapper.getUserByZFB(zfbOpenId);
    }

    @Override
    public BaseUser getUserByQQ(String qqOpenId) {
        return permMapper.getUserByQQ(qqOpenId);
    }

    @Override
    public BaseUser getUserBySina(String sinaOpenId) {
        return permMapper.getUserBySina(sinaOpenId);
    }
}

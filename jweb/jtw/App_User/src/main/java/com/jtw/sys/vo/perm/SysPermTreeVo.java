package com.jtw.sys.vo.perm;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * DESCRIPT:
 *
 * @author cjsky666
 * @date 2018/10/30 11:10
 */
@Data
public class SysPermTreeVo implements Serializable {
    private static final long serialVersionUID = 6643032242655223514L;
    private Integer group;
    private String desc;
    private List<SysPermMarkVo> perms;
}

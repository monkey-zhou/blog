package com.jtw.sys.anno;

import com.jtw.common.bean.enums.ReqCode;
import com.jtw.common.org.Sid;
import com.jtw.sys.constants.Constant;
import com.jtw.sys.emuns.AssetChangeCode;
import com.jtw.sys.mapper.member.MemberAssetLogMapper;
import com.jtw.sys.mapper.member.MemberAssetMapper;
import com.jtw.sys.mapper.member.MemberCheckInMapper;
import com.jtw.sys.mapper.member.MemberCheckInRewardMapper;
import com.jtw.sys.model.log.TSysUserLoginLog;
import com.jtw.sys.model.member.TSysMemberAsset;
import com.jtw.sys.model.member.TSysMemberAssetLog;
import com.jtw.sys.model.member.TSysMemberCheckIn;
import com.jtw.sys.model.member.TSysMemberCheckInReward;
import com.jtw.sys.model.user.TSysUser;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.SecurityUtils;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;

/**
 * DESCRIPT: 登陆奖励发放注解功能
 *
 * @author cjsky666
 * @date 2019/8/8 15:19
 */
@Component
@Slf4j
@Aspect
public class LoginRewardAnno {
    @Autowired
    private Sid sid;
    @Autowired
    private MemberCheckInMapper memberCheckInMapper;
    @Autowired
    private MemberCheckInRewardMapper memberCheckInRewardMapper;
    @Autowired
    private MemberAssetLogMapper memberAssetLogMapper;
    @Autowired
    MemberAssetMapper memberAssetMapper;

    @After(value = ("execution(* com.jtw.sys.mapper.log.SysLoginMapper.insertSelective(..)) && args(recoed)"))
    public void checkAccsessLog(JoinPoint joinPoint,TSysUserLoginLog recoed) {
        if(recoed!=null){
            log.debug("当前登陆的用户是，"+recoed.getUserId().toString());

            if(memberCheckInMapper.getTodayCheckIn(recoed.getUserId(),LocalDate.now().toDate())<(Integer.valueOf(Constant.sysConfMap.get(Constant.KEY_CHECK_IN_COUNT)))){
                log.debug("可以签到");
                log.debug("发放奖励--》开始");
                //增加账户积分
                TSysMemberAsset asset = memberAssetMapper.selectByPrimaryKey(recoed.getUserId());
                Integer rewardNum = Integer.valueOf(Constant.sysConfMap.get(Constant.KEY_CHECK_IN_REWARD));
                asset.setBalance(asset.getBalance()+rewardNum);
                if(memberAssetMapper.updateByVersion(asset)!=1){
                    throw ReqCode.DataLockedUpdateError.getException();
                }

                TSysMemberCheckInReward tSysMemberCheckInReward = new TSysMemberCheckInReward();
                //更新签到记录
                TSysMemberCheckIn tSysMemberCheckIn = new TSysMemberCheckIn();
                tSysMemberCheckIn.setCheckInDate(LocalDate.now().toDate());
                tSysMemberCheckIn.setUserId(recoed.getUserId());
                tSysMemberCheckIn.setCheckInDate(LocalDateTime.now().toDate());
                tSysMemberCheckIn.setUserName(SecurityUtils.getSubject().getSession().getAttribute(Constant.KEY_USER_USER_NAME).toString());
                memberCheckInMapper.insertSelective(tSysMemberCheckIn);

                //更新签到详情记录
                tSysMemberCheckInReward.setCheckInId(tSysMemberCheckIn.getId());
                tSysMemberCheckInReward.setCheckInRewardBalance(rewardNum);
                tSysMemberCheckInReward.setOperator("admin");
                tSysMemberCheckInReward.setOperatorIp("0.0.0.0");
                tSysMemberCheckInReward.setState(1);
                memberCheckInRewardMapper.insertSelective(tSysMemberCheckInReward);

                //更新账户积分账变记录
                TSysMemberAssetLog tSysMemberAssetLog = new TSysMemberAssetLog();
                tSysMemberAssetLog.setBalance(rewardNum);
                tSysMemberAssetLog.setAssetBalance(asset.getBalance());
                tSysMemberAssetLog.setAssetFreezeBalance(asset.getFreezeBalance());
                tSysMemberAssetLog.setType(AssetChangeCode.ADD_CHECK_IN.getType());
                tSysMemberAssetLog.setDescript(AssetChangeCode.ADD_CHECK_IN.getDescript());
                tSysMemberAssetLog.setOperatorIp("0.0.0.0");
                tSysMemberAssetLog.setOperator(SecurityUtils.getSubject().getSession().getAttribute(Constant.KEY_USER_USER_NAME).toString());
                tSysMemberAssetLog.setSerialNo(sid.nextShort());
                tSysMemberAssetLog.setUserId(recoed.getUserId());
                memberAssetLogMapper.insertSelective(tSysMemberAssetLog);
                log.debug("发放奖励--》结束");
            }
        }
    }
}

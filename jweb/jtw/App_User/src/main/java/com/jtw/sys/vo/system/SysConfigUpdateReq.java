package com.jtw.sys.vo.system;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * DESCRIPT:
 *
 * @author cjsky666
 * @date 2019/1/18 16:21
 */
@Data
public class SysConfigUpdateReq implements Serializable {
    private static final long serialVersionUID = 5465093668183618425L;
    @NotNull(message = "id不能为空")
    private Integer id;
    @NotBlank(message = "值不能为空")
    private String keyValue;
    @NotBlank(message = "描述不能为空")
    private String descript;
}

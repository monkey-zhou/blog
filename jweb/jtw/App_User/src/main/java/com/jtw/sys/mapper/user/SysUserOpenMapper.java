package com.jtw.sys.mapper.user;

import com.MyMapper;
import com.jtw.sys.model.user.TSysUserOpen;

/**
 * DESCRIPT:
 *
 * @author cjsky666
 * @date 2019/8/5 13:11
 */
public interface SysUserOpenMapper extends MyMapper<TSysUserOpen> {
}

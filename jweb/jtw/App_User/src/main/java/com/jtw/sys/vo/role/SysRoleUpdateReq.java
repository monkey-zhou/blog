package com.jtw.sys.vo.role;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * DESCRIPT:添加系统角色请求模型
 *
 * @author cjsky666
 * @date 2018/9/17 16:38
 */
@Data
public class SysRoleUpdateReq implements Serializable {
    private static final long serialVersionUID = -2745018956786586420L;
    @NotNull(message = "角色id不能为空")
    private Integer id;
    @NotNull(message = "角色名称不能为空")
    @Length(max = 45,message = "角色名称过长")
    private String name;
    @NotBlank(message = "角色备注不能为空")
    @Length(max = 100,message = "角色备注过长")
    private String desc;
}

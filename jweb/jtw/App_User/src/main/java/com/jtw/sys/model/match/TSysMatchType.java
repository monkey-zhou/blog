package com.jtw.sys.model.match;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

/**
 * DESCRIPT:赛事类型表
 *
 * @author cjsky666
 * @date 2019/7/18 10:31
 */
@Data
@Table(name="`t_sys_match_type`")
public class TSysMatchType implements Serializable {
    private static final long serialVersionUID = 2366694473232846855L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "SELECT LAST_INSERT_ID() from dual")
    private Integer id;
    private String name;
    private String image;
    private String ThumbNailImage;
    private String operatorIp;
    private String operator;
    @JsonFormat(pattern="yyyy/MM/dd HH:mm:ss",timezone="GMT+8")
    private Date createTime;
    @JsonFormat(pattern="yyyy/MM/dd HH:mm:ss",timezone="GMT+8")
    private Date updateTime;
}

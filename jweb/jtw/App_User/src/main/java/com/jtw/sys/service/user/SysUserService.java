package com.jtw.sys.service.user;

import com.github.pagehelper.Page;
import com.jtw.common.baseService.BaseService;
import com.jtw.common.bean.vo.FindPagingVo;
import com.jtw.sys.model.user.TSysUser;
import com.jtw.sys.model.user.TSysUserInfo;
import com.jtw.sys.vo.member.*;
import com.jtw.sys.vo.role.SysRoleUpdateVo;
import com.jtw.sys.vo.user.SysUserAccountPermVo;
import com.jtw.sys.vo.user.SysUserAddReq;
import com.jtw.sys.vo.user.SysUserInfoBaseVo;
import com.jtw.sys.vo.user.UserWithdrawPwdResetVo;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * DESCRIPT:
 *
 * @author cjsky666
 * @date 2018-8-24 14:15
 */
public interface SysUserService extends BaseService<TSysUser> {
    /**
     * 根据用户名查询用户基本业务信息
     * @param userName
     */
    SysUserInfoBaseVo getUserInfoBaseVoByUserName(String userName);

    Page<SysUserInfoBaseVo> findAllUserInfoBaseVo(FindPagingVo pagingVo);

    /**
     * 更新当前用户的登录时间
     * @param id
     */
    void updateUserLoginTime(Long id);

    /**
     * 新增用户账号
      * @param sysUserAddReq
     */
    void addSysUserAccount(SysUserAddReq sysUserAddReq,HttpServletRequest request);
    /**
     * 锁定账户
     * @param userId
     */
    void lockedSysUserAccount(Long userId);
    /**
     * 解锁账户
     * @param userId
     */
    void unlockedSysUserAccount(Long userId);

    /**
     * 根据id修改账户的角色关系
      * @param id
     * @param roleList
     */
    void modifyUserRolesByUserId(Long id, Integer[] roleList,HttpServletRequest request);

    void modifySysUserBaseInfoByUserId(TSysUserInfo sysUserInfo);

    /**
     * 根据账号id查询所拥有的角色
     * @param userId
     * @return
     */
    List<SysRoleUpdateVo> getUserRolesByUserId(Long userId);

    /**
     * 根据账号id查询未拥有的系统角色
     * @param userId
     * @return
     */
    List<SysRoleUpdateVo> getAllRolesByUserIdNotHave(Long userId);

    /**
     * 根据账号id查询所有的角色及角色的权限
     * @param userId
     * @return
     */
    List<SysUserAccountPermVo> getAllPermsBySysUserId(Long userId);

    /**
     *修改用户密码
     * @param userId 用户id
     * @param newPassword 新密码
     */
    void modifyAccountPwdByUserId(Long userId, String newPassword);
    /**
     * 判断账号是否为管理员
     */
    Boolean isAdmin(Long userId);

    Page<SysUserInfoBaseVo> findAllAdminUserInfoBaseVo(FindPagingVo findPagingVo);

    /**
     * 修改自己的登陆密码
     * @param request
     * @param memberPwdReq
     */
    void modifyMemberPwd(HttpServletRequest request, MemberPwdReq memberPwdReq);

    /**
     * 重置密码申请
     * @param request
     * @param memberPwdResetReq
     */
    void MemberPwdResetReq(HttpServletRequest request, MemberPwdResetReq memberPwdResetReq);

    Page<UserWithdrawPwdResetVo> findAllPwdResetApply(FindPagingVo findPagingVo);


    void confirmPwdResetApply(HttpServletRequest request,Long id);

    void refusedPwdResetApply(HttpServletRequest request, Long id,String reason);

    SysUserInfoBaseVo getUserInfoBaseVoByUserId(Long userId);

    /**
     * 检查今天是否签到了，返回0和1
     * @param userId
     * @return
     */
    Integer getTodayCheckIn(Long userId);

    /**
     * 会员签到
     * @param userId
     */
    void memberCheckIn(Long userId);

    /**
     * 查询会员签到记录
     * @param findPagingVo
     * @return
     */
    Page<MemberCheckInVo> findAllCheckInReward(FindPagingVo findPagingVo);

    /**
     * 微信公众号授权登陆
     * @param code
     * @return
     */
    String checkOpenCodeInfo(String code,HttpServletRequest request);
}

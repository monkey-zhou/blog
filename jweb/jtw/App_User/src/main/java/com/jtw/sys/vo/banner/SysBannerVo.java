package com.jtw.sys.vo.banner;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.Date;

/**
 * DESCRIPT:
 *
 * @author cjsky666
 * @date 2019/2/21 11:58
 */
@Data
public class SysBannerVo implements Serializable {
    private static final long serialVersionUID = -7986472830642630915L;
    private Integer id;
    private String title;
    private String imagePath;
    private String thumbNailImagePath;
    private String url;
    private Integer skipWay;
    private Integer type;
    private Integer state;
    @JsonFormat(pattern="yyyy/MM/dd HH:mm:ss",timezone="GMT+8")
    private Date createTime;
}

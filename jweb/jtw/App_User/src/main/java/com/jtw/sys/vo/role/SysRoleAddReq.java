package com.jtw.sys.vo.role;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * DESCRIPT:添加角色业务模型请求类型
 *
 * @author cjsky666
 * @date 2018/9/18 17:08
 */
@Data
public class SysRoleAddReq implements Serializable {
    private static final long serialVersionUID = -2913856555893937360L;
    @NotBlank(message = "角色名称不能为空")
    @Length(max = 45,message = "角色名称过长")
    private String name;
    @NotBlank(message = "角色备注不能为空")
    @Length(max = 100,message = "角色备注过长")
    private String desc;
}

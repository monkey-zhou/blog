package com.jtw.sys.service.member;

import com.github.pagehelper.Page;
import com.jtw.common.baseService.BaseService;
import com.jtw.common.bean.vo.FindPagingVo;
import com.jtw.common.bean.vo.PagingVo;
import com.jtw.sys.model.member.TSysMemberAsset;
import com.jtw.sys.vo.asset.SysMemberAssetUpdateReq;
import com.jtw.sys.vo.asset.SysMemberAssetVo;
import com.jtw.sys.vo.member.MemberAssetLogVo;
import com.jtw.sys.vo.member.MemberAssetRankVo;
import com.jtw.sys.vo.member.MemberAssetVo;
import com.jtw.sys.vo.member.SysMemberAssetLogVo;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * DESCRIPT:
 *
 * @author cjsky666
 * @date 2019/1/19 20:44
 */
public interface MemberAssetService extends BaseService<TSysMemberAsset> {
    /**
     * 会员获取资金信息
     * @param userId
     * @return
     */
    MemberAssetVo getOneByUserId(Long userId);

    /**
     * 会员查看资金账变记录
     * @param userId
     * @param findPagingVo
     * @return
     */
    Page<MemberAssetLogVo> findAllMyAssetLog(Long userId, FindPagingVo findPagingVo);


    /**
     * 查询所有的会员账变记录
     * @param findPagingVo
     * @return
     */
    Page<SysMemberAssetLogVo> findAllMemberAssetChangeLog(FindPagingVo findPagingVo);

    /**
     * 查询系统会员资金信息
     * @param findPagingVo
     * @return
     */
    Page<SysMemberAssetVo> findAllMemberAssetDetail(FindPagingVo findPagingVo);

    /**
     * 调整会员资金
     * @param request
     * @param sysMemberAssetUpdateReq
     */
    void adjustMemberAsset(HttpServletRequest request, SysMemberAssetUpdateReq sysMemberAssetUpdateReq);

    Page<MemberAssetRankVo> findAllRank(PagingVo pagingVo);

    /**
     * 查询我的积分排名
     * @param userId
     * @return
     */
    MemberAssetRankVo getMyAssetRank(Integer userId);

    /**
     * 微信公众号分享回调
     * @param userId
     */
    void wxgzhShareCallBack(Long userId);
}

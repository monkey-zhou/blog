package com.jtw.sys.service.user;

import com.github.pagehelper.Page;
import com.jtw.common.baseService.BaseService;
import com.jtw.common.bean.vo.PagingVo;
import com.jtw.sys.model.user.TSysRole;
import com.jtw.sys.vo.role.SysRoleUpdateVo;

/**
 * DESCRIPT:
 *
 * @author cjsky666
 * @date 2018-8-24 14:15
 */
public interface SysRoleService extends BaseService<TSysRole> {

    Page<SysRoleUpdateVo> findAllRoles(PagingVo pagingVo);

    /**
     * 删除角色的话会删除对应的所有关联权限及角色关联的用户表中的数据
     * @param roleId
     */
    void deleteAllOfThisRolePerms(Integer roleId);
}

package com.jtw.sys.vo.member;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Max;
import java.io.Serializable;

/**
 * DESCRIPT:
 *
 * @author cjsky666
 * @date 2019/2/18 17:47
 */
@Data
public class MemberPwdResetReq implements Serializable {
    private static final long serialVersionUID = 6973174024799213706L;
    @Length(max=45,message = "最多不能超过45个字")
    private String reason;
    private String password;
}

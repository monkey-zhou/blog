package com.jtw.sys.listener;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PreDestroy;

/**
 * @implNote 系统停止时的监控任务
 * @author cjsky666
 * @date 2018-8-21 15:38
 *
 */
@Configuration
@Slf4j
public class SystemExit {
    @PreDestroy
    public void destroy() {
        log.info("系统正在销毁");
        log.info("系统已关闭~~~");
    }

}

package com.jtw.sys.model.perm;

import lombok.Data;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * DESCRIPT: 权限设定组表的实体
 *
 * @author cjsky666
 * @date 2018/11/15 14:19
 */
@Data
@Table(name = "`t_sys_perm_group`")
public class TSysPermGroup implements Serializable {
    private static final long serialVersionUID = 3914573991029339885L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "SELECT LAST_INSERT_ID() from dual")
    private Integer id;
    private Integer group;
    private String desc;
}

package com.jtw.sys.model.task;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

/**
 * DESCRIPT:
 *
 * @author cjsky666
 * @date 2018/11/23 10:10
 */
@Data
@Table(name = "`t_sys_task`")
public class TSysTask implements Serializable {
    private static final long serialVersionUID = -4006501518057372504L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "SELECT LAST_INSERT_ID() from dual")
    private Integer id;
    private String name;
    private Integer gameId;
    private String groupName;
    private String cron;
    private Boolean taskState;
    private Boolean runState;
    private String param;
    private String methodName;
    private String descript;
    private String beanClass;
    private Long createId;
    private Long updateId;
    @JsonFormat(pattern="yyyy/MM/dd HH:mm:ss",timezone="GMT+8")
    private Date createTime;
    @JsonFormat(pattern="yyyy/MM/dd HH:mm:ss",timezone="GMT+8")
    private Date updateTime;
}

package com.jtw.sys.model.match;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

/**
 * DESCRIPT: 赛事竞猜表
 * @author cjsky666
 * @date 2019/7/18 10:47
 */
@Data
@Table(name = "`t_sys_match_bet`")
public class TSysMatchBet implements Serializable {
    private static final long serialVersionUID = 1151349114001893299L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "SELECT LAST_INSERT_ID() from dual")
    private Integer id;
    private Integer userId;
    private String userName;
    private String nickName;
    private Integer matchId;
    private String matchNickName;
    private Integer scheduleId;
    private String scheduleNickName;
    private Integer homeTeamId;
    private String homeTeamNickName;
    private Integer awayTeamId;
    private String awayTeamNickName;
    private Integer betTeamId;
    private Integer versusId;
    private String betTeamNickName;
    private Integer winTeamId;
    private String winTeamNickName;
    private Integer homeTeamScore;
    private Integer awayTeamScore;
    private Integer winState;
    private Integer betBalance;
    private Integer winBalance;
    private Integer odds;
    private String operator;
    @JsonFormat(pattern="yyyy/MM/dd HH:mm:ss",timezone="GMT+8")
    private Date createTime;
    @JsonFormat(pattern="yyyy/MM/dd HH:mm:ss",timezone="GMT+8")
    private Date updateTime;
}

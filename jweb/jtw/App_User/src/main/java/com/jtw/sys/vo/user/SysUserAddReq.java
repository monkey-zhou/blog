package com.jtw.sys.vo.user;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Max;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * DESCRIPT: 添加系统用户账号
 * @author cjsky666
 * @date 2018/9/21 16:44
 */
@Data
public class SysUserAddReq implements Serializable {
    private static final long serialVersionUID = 3795569411892002187L;
    @Length(min=6,max = 20,message = "账号长度6-20")
    @NotBlank(message = "用户名不能为空")
    private String userName;

    @NotBlank(message = "密码不能为空")
    private String password;

    private String inviteCode;//邀请码
    private String nickName;//昵称

    private String headImage;

//    @NotEmpty(message = "角色不能为空")
//    private Integer [] roles;

//    @NotBlank(message = "用户描述信息不能为空")
//    private String desc;
}

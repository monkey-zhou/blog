package com.jtw.sys.vo.perm;

import lombok.Data;

import java.io.Serializable;

/**
 * DESCRIPT:系统角色权限业务模型
 *
 * @author cjsky666
 * @date 2018/9/18 16:42
 */
@Data
public class SysRolePermVo implements Serializable {

    private static final long serialVersionUID = -8693754287327610428L;
    private Integer id;

    private String name;

    private Integer type;

    private String desc;

    private Integer group;

    private Integer sort;

}

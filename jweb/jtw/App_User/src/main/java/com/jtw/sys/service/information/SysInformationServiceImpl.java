package com.jtw.sys.service.information;

import com.github.pagehelper.Page;
import com.jtw.common.bean.vo.FindPagingVo;
import com.jtw.common.bean.vo.PagingVo;
import com.jtw.common.util.PagingUtil;
import com.jtw.sys.mapper.information.SysInformationMapper;
import com.jtw.sys.model.information.TSysInformation;
import com.jtw.sys.vo.information.SysInformationLessVo;
import com.jtw.sys.vo.information.SysInformationVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * DESCRIPT:
 *
 * @author cjsky666
 * @date 2019/2/21 11:49
 */
@Service
public class SysInformationServiceImpl implements SysInformationService {

    @Autowired
    private SysInformationMapper sysInformationMapper;
    @Override
    public void save(TSysInformation tSysInformation) {
        sysInformationMapper.insertSelective(tSysInformation);
    }

    @Override
    public void saveAll(List<TSysInformation> list) {

    }

    @Override
    public void update(TSysInformation tSysInformation) {
        sysInformationMapper.updateByPrimaryKeySelective(tSysInformation);
    }

    @Override
    public void delete(Object id) {

    }

    @Override
    public TSysInformation getById(Object id) {
        return null;
    }

    @Override
    public Page<TSysInformation> findAll(PagingVo pagingVo) {
        return null;
    }

    @Override
    public Page<TSysInformation> findAll(FindPagingVo findPagingVo) {
        PagingUtil.start(findPagingVo);
        return sysInformationMapper.findAll(PagingUtil.buildWhereSQLToString(findPagingVo.getFindParams()));
    }

    @Override
    public List<TSysInformation> getAll() {
        return null;
    }

    @Override
    public SysInformationVo getInformationById(Integer id) {
        return sysInformationMapper.getInformationById(id);
    }

    @Override
    public Page<SysInformationLessVo> findAllSysInformation(FindPagingVo findPagingVo) {
        PagingUtil.start(findPagingVo);
        return sysInformationMapper.findAllSysInformation(PagingUtil.buildWhereSQLToString(findPagingVo.getFindParams()));
    }
}

package com.jtw.sys.mapper.menu;

import com.MyMapper;
import com.github.pagehelper.Page;
import com.jtw.sys.model.menu.TSysMenu;
import com.jtw.sys.vo.menu.SysMenuMarkVo;
import com.jtw.sys.vo.menu.SysMenuRoleMarkModifyReq;
import com.jtw.sys.vo.menu.SysMenuTreeVo;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * DESCRIPT:
 *
 * @author cjsky666
 * @date 2018/10/10 14:03
 */
public interface SysMenuMapper extends MyMapper<TSysMenu> {

    List<SysMenuMarkVo> getAllSysMenuTreeMarkByRoleId(@Param("roleId") Integer roleId);

    @Select("SELECT ID FROM `t_sys_menu` WHERE ID NOT IN (SELECT ID FROM `ct_sys_role_menu` WHERE ROLE_ID = #{roleId});")
    Integer [] getAllMenusByRoleIdNotHave(@Param("roleId") Integer roleId);

    @Select("SELECT * FROM `t_sys_menu`")
    Page<TSysMenu> findAll();

    @Delete("DELETE FROM `ct_sys_role_menu` WHERE ROLE_ID = #{roleId};")
    Integer clearRoleMenusByRoleId(Integer roleId);

    Integer batchSaveRoleMenusByRoleId(@Param("sysMenuRoleMarkModifyReq") SysMenuRoleMarkModifyReq sysMenuRoleMarkModifyReq);

    @Select("SELECT `PARENT` FROM `t_sys_menu` WHERE ID = #{id};")
    Integer getAllMenuParentIdByMenuId(@Param("id")Integer id);

    Integer[] getAllRoleMenuIdByUserId(@Param("userId")Long userId);

    List<SysMenuTreeVo> getAllMenusTreeByUserId(@Param("userId") Long userId);
}

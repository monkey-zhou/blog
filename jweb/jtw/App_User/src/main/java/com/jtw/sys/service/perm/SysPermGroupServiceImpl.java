package com.jtw.sys.service.perm;

import com.github.pagehelper.Page;
import com.jtw.common.bean.vo.FindPagingVo;
import com.jtw.common.bean.vo.PagingVo;
import com.jtw.sys.mapper.perm.SysPermGroupMapper;
import com.jtw.sys.model.perm.TSysPermGroup;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * DESCRIPT:
 *
 * @author cjsky666
 * @date 2018/9/12 13:56
 */
@Service
public class SysPermGroupServiceImpl implements SysPermGroupService {
    @Autowired
    private SysPermGroupMapper sysPermGroupMapper;
    @Override
    public void save(TSysPermGroup tSysPermGroup) {
        sysPermGroupMapper.insertSelective(tSysPermGroup);
    }

    @Override
    @Transactional
    public void saveAll(List<TSysPermGroup> list) {
        sysPermGroupMapper.insertList(list);
    }

    @Override
    public void update(TSysPermGroup tSysPermGroup) {

    }

    @Override
    public void delete(Object id) {

    }

    @Override
    public TSysPermGroup getById(Object id) {
        return null;
    }

    @Override
    public Page<TSysPermGroup> findAll(PagingVo pagingVo) {
        return null;
    }

    @Override
    public Page<TSysPermGroup> findAll(FindPagingVo findPagingVo) {
        return null;
    }

    @Override
    public List<TSysPermGroup> getAll() {
        return sysPermGroupMapper.selectAll();
    }
}

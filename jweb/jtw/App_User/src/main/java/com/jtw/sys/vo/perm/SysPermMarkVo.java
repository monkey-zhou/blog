package com.jtw.sys.vo.perm;

import lombok.Data;

import java.io.Serializable;

/**
 * DESCRIPT:
 *
 * @author cjsky666
 * @date 2018/10/30 10:50
 */
@Data
public class SysPermMarkVo implements Serializable {
    private static final long serialVersionUID = 8259106450482748826L;
    private Integer id;
    private String desc;
    private Boolean mark;
}

package com.jtw.sys.vo.banner;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * DESCRIPT:
 *
 * @author cjsky666
 * @date 2019/2/21 11:46
 */
@Data
public class SysBannerUpdateReq implements Serializable {
    private static final long serialVersionUID = 8775208825768869485L;
    @NotNull(message = "id不能为空")
    private Integer id;
    private String title;
    @NotBlank(message = "原始图片不能为空")
    private String imagePath;
    @NotBlank(message = "缩略图片不能为空")
    private String thumbNailImagePath;
    private String url;
    @NotNull(message = "跳转方式不能为空")
    private Integer skipWay;
    @NotNull(message = "轮播图类型不能为空")
    private Integer type;
    @NotNull(message = "审核状态不能为空")
    private Integer state ;
}

/*
 Navicat Premium Data Transfer

 Source Server         : 本地数据库
 Source Server Type    : MySQL
 Source Server Version : 50724
 Source Host           : localhost:3306
 Source Schema         : test4

 Target Server Type    : MySQL
 Target Server Version : 50724
 File Encoding         : 65001

 Date: 18/12/2019 12:25:10
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for ct_sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `ct_sys_role_menu`;
CREATE TABLE `ct_sys_role_menu`  (
  `ROLE_ID` int(11) UNSIGNED NOT NULL COMMENT '用户ID',
  `MENU_ID` int(11) UNSIGNED NOT NULL COMMENT '角色ID',
  `OPERATOR` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '操作者',
  `OPERATOR_IP` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '最后一次更新者的ip地址',
  `CREATE_TIME` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) COMMENT '添加时间',
  `UPDATE_TIME` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3) COMMENT '最后更新时间',
  PRIMARY KEY (`ROLE_ID`, `MENU_ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '系统角色-菜单关联表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of ct_sys_role_menu
-- ----------------------------
INSERT INTO `ct_sys_role_menu` VALUES (1, 1, '', '', '2019-12-18 12:23:23.730', '2019-12-18 12:23:23.730');
INSERT INTO `ct_sys_role_menu` VALUES (1, 2, '', '', '2019-12-18 12:23:23.730', '2019-12-18 12:23:23.730');
INSERT INTO `ct_sys_role_menu` VALUES (1, 3, '', '', '2019-12-18 12:23:23.730', '2019-12-18 12:23:23.730');
INSERT INTO `ct_sys_role_menu` VALUES (1, 5, '', '', '2019-12-18 12:23:23.730', '2019-12-18 12:23:23.730');
INSERT INTO `ct_sys_role_menu` VALUES (1, 7, '', '', '2019-12-18 12:23:23.730', '2019-12-18 12:23:23.730');
INSERT INTO `ct_sys_role_menu` VALUES (1, 9, '', '', '2019-12-18 12:23:23.730', '2019-12-18 12:23:23.730');
INSERT INTO `ct_sys_role_menu` VALUES (1, 11, '', '', '2019-12-18 12:23:23.730', '2019-12-18 12:23:23.730');
INSERT INTO `ct_sys_role_menu` VALUES (1, 19, '', '', '2019-12-18 12:23:23.730', '2019-12-18 12:23:23.730');
INSERT INTO `ct_sys_role_menu` VALUES (1, 20, '', '', '2019-12-18 12:23:23.730', '2019-12-18 12:23:23.730');
INSERT INTO `ct_sys_role_menu` VALUES (1, 23, '', '', '2019-12-18 12:23:23.730', '2019-12-18 12:23:23.730');
INSERT INTO `ct_sys_role_menu` VALUES (1, 32, '', '', '2019-12-18 12:23:23.730', '2019-12-18 12:23:23.730');
INSERT INTO `ct_sys_role_menu` VALUES (1, 33, '', '', '2019-12-18 12:23:23.730', '2019-12-18 12:23:23.730');
INSERT INTO `ct_sys_role_menu` VALUES (1, 34, '', '', '2019-12-18 12:23:23.730', '2019-12-18 12:23:23.730');
INSERT INTO `ct_sys_role_menu` VALUES (1, 37, '', '', '2019-12-18 12:23:23.730', '2019-12-18 12:23:23.730');
INSERT INTO `ct_sys_role_menu` VALUES (1, 38, '', '', '2019-12-18 12:23:23.730', '2019-12-18 12:23:23.730');
INSERT INTO `ct_sys_role_menu` VALUES (1, 39, '', '', '2019-12-18 12:23:23.730', '2019-12-18 12:23:23.730');
INSERT INTO `ct_sys_role_menu` VALUES (1, 40, '', '', '2019-12-18 12:23:23.730', '2019-12-18 12:23:23.730');
INSERT INTO `ct_sys_role_menu` VALUES (1, 44, '', '', '2019-12-18 12:23:23.730', '2019-12-18 12:23:23.730');
INSERT INTO `ct_sys_role_menu` VALUES (1, 45, '', '', '2019-12-18 12:23:23.730', '2019-12-18 12:23:23.730');
INSERT INTO `ct_sys_role_menu` VALUES (1, 46, '', '', '2019-12-18 12:23:23.730', '2019-12-18 12:23:23.730');
INSERT INTO `ct_sys_role_menu` VALUES (1, 56, '', '', '2019-12-18 12:23:23.730', '2019-12-18 12:23:23.730');
INSERT INTO `ct_sys_role_menu` VALUES (1, 57, '', '', '2019-12-18 12:23:23.730', '2019-12-18 12:23:23.730');
INSERT INTO `ct_sys_role_menu` VALUES (1, 58, '', '', '2019-12-18 12:23:23.730', '2019-12-18 12:23:23.730');
INSERT INTO `ct_sys_role_menu` VALUES (1, 60, '', '', '2019-12-18 12:23:23.730', '2019-12-18 12:23:23.730');

-- ----------------------------
-- Table structure for ct_sys_role_perm
-- ----------------------------
DROP TABLE IF EXISTS `ct_sys_role_perm`;
CREATE TABLE `ct_sys_role_perm`  (
  `ROLE_ID` int(11) UNSIGNED NOT NULL COMMENT '角色ID',
  `PERM_ID` int(11) UNSIGNED NOT NULL COMMENT '权限ID',
  `OPERATOR` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '操作者',
  `OPERATOR_IP` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '最后一次更新者的ip地址',
  `CREATE_TIME` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) COMMENT '添加时间',
  `UPDATE_TIME` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3) COMMENT '最后更新时间',
  PRIMARY KEY (`ROLE_ID`, `PERM_ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '系统角色-权限关联表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of ct_sys_role_perm
-- ----------------------------
INSERT INTO `ct_sys_role_perm` VALUES (1, 1, '', '', '2019-12-18 12:23:23.531', '2019-12-18 12:23:23.531');
INSERT INTO `ct_sys_role_perm` VALUES (1, 2, '', '', '2019-12-18 12:23:23.531', '2019-12-18 12:23:23.531');
INSERT INTO `ct_sys_role_perm` VALUES (1, 3, '', '', '2019-12-18 12:23:23.531', '2019-12-18 12:23:23.531');
INSERT INTO `ct_sys_role_perm` VALUES (1, 4, '', '', '2019-12-18 12:23:23.531', '2019-12-18 12:23:23.531');
INSERT INTO `ct_sys_role_perm` VALUES (1, 5, '', '', '2019-12-18 12:23:23.531', '2019-12-18 12:23:23.531');
INSERT INTO `ct_sys_role_perm` VALUES (1, 6, '', '', '2019-12-18 12:23:23.531', '2019-12-18 12:23:23.531');
INSERT INTO `ct_sys_role_perm` VALUES (1, 7, '', '', '2019-12-18 12:23:23.531', '2019-12-18 12:23:23.531');
INSERT INTO `ct_sys_role_perm` VALUES (1, 8, '', '', '2019-12-18 12:23:23.531', '2019-12-18 12:23:23.531');
INSERT INTO `ct_sys_role_perm` VALUES (1, 9, '', '', '2019-12-18 12:23:23.531', '2019-12-18 12:23:23.531');
INSERT INTO `ct_sys_role_perm` VALUES (1, 10, '', '', '2019-12-18 12:23:23.531', '2019-12-18 12:23:23.531');
INSERT INTO `ct_sys_role_perm` VALUES (1, 11, '', '', '2019-12-18 12:23:23.531', '2019-12-18 12:23:23.531');
INSERT INTO `ct_sys_role_perm` VALUES (1, 12, '', '', '2019-12-18 12:23:23.531', '2019-12-18 12:23:23.531');
INSERT INTO `ct_sys_role_perm` VALUES (1, 13, '', '', '2019-12-18 12:23:23.531', '2019-12-18 12:23:23.531');
INSERT INTO `ct_sys_role_perm` VALUES (1, 14, '', '', '2019-12-18 12:23:23.531', '2019-12-18 12:23:23.531');
INSERT INTO `ct_sys_role_perm` VALUES (1, 15, '', '', '2019-12-18 12:23:23.531', '2019-12-18 12:23:23.531');
INSERT INTO `ct_sys_role_perm` VALUES (1, 16, '', '', '2019-12-18 12:23:23.531', '2019-12-18 12:23:23.531');
INSERT INTO `ct_sys_role_perm` VALUES (1, 17, '', '', '2019-12-18 12:23:23.531', '2019-12-18 12:23:23.531');
INSERT INTO `ct_sys_role_perm` VALUES (1, 19, '', '', '2019-12-18 12:23:23.531', '2019-12-18 12:23:23.531');
INSERT INTO `ct_sys_role_perm` VALUES (1, 22, '', '', '2019-12-18 12:23:23.531', '2019-12-18 12:23:23.531');
INSERT INTO `ct_sys_role_perm` VALUES (1, 23, '', '', '2019-12-18 12:23:23.531', '2019-12-18 12:23:23.531');
INSERT INTO `ct_sys_role_perm` VALUES (1, 24, '', '', '2019-12-18 12:23:23.531', '2019-12-18 12:23:23.531');
INSERT INTO `ct_sys_role_perm` VALUES (1, 25, '', '', '2019-12-18 12:23:23.531', '2019-12-18 12:23:23.531');
INSERT INTO `ct_sys_role_perm` VALUES (1, 26, '', '', '2019-12-18 12:23:23.531', '2019-12-18 12:23:23.531');
INSERT INTO `ct_sys_role_perm` VALUES (1, 27, '', '', '2019-12-18 12:23:23.531', '2019-12-18 12:23:23.531');
INSERT INTO `ct_sys_role_perm` VALUES (1, 28, '', '', '2019-12-18 12:23:23.531', '2019-12-18 12:23:23.531');
INSERT INTO `ct_sys_role_perm` VALUES (1, 29, '', '', '2019-12-18 12:23:23.531', '2019-12-18 12:23:23.531');
INSERT INTO `ct_sys_role_perm` VALUES (1, 30, '', '', '2019-12-18 12:23:23.531', '2019-12-18 12:23:23.531');
INSERT INTO `ct_sys_role_perm` VALUES (1, 31, '', '', '2019-12-18 12:23:23.531', '2019-12-18 12:23:23.531');
INSERT INTO `ct_sys_role_perm` VALUES (1, 32, '', '', '2019-12-18 12:23:23.531', '2019-12-18 12:23:23.531');
INSERT INTO `ct_sys_role_perm` VALUES (1, 33, '', '', '2019-12-18 12:23:23.531', '2019-12-18 12:23:23.531');
INSERT INTO `ct_sys_role_perm` VALUES (1, 34, '', '', '2019-12-18 12:23:23.531', '2019-12-18 12:23:23.531');
INSERT INTO `ct_sys_role_perm` VALUES (1, 35, '', '', '2019-12-18 12:23:23.531', '2019-12-18 12:23:23.531');
INSERT INTO `ct_sys_role_perm` VALUES (1, 36, '', '', '2019-12-18 12:23:23.531', '2019-12-18 12:23:23.531');
INSERT INTO `ct_sys_role_perm` VALUES (1, 37, '', '', '2019-12-18 12:23:23.531', '2019-12-18 12:23:23.531');
INSERT INTO `ct_sys_role_perm` VALUES (1, 38, '', '', '2019-12-18 12:23:23.531', '2019-12-18 12:23:23.531');
INSERT INTO `ct_sys_role_perm` VALUES (1, 39, '', '', '2019-12-18 12:23:23.531', '2019-12-18 12:23:23.531');
INSERT INTO `ct_sys_role_perm` VALUES (1, 40, '', '', '2019-12-18 12:23:23.531', '2019-12-18 12:23:23.531');
INSERT INTO `ct_sys_role_perm` VALUES (1, 41, '', '', '2019-12-18 12:23:23.531', '2019-12-18 12:23:23.531');
INSERT INTO `ct_sys_role_perm` VALUES (1, 42, '', '', '2019-12-18 12:23:23.531', '2019-12-18 12:23:23.531');
INSERT INTO `ct_sys_role_perm` VALUES (1, 43, '', '', '2019-12-18 12:23:23.531', '2019-12-18 12:23:23.531');
INSERT INTO `ct_sys_role_perm` VALUES (1, 44, '', '', '2019-12-18 12:23:23.531', '2019-12-18 12:23:23.531');
INSERT INTO `ct_sys_role_perm` VALUES (1, 45, '', '', '2019-12-18 12:23:23.531', '2019-12-18 12:23:23.531');
INSERT INTO `ct_sys_role_perm` VALUES (1, 47, '', '', '2019-12-18 12:23:23.531', '2019-12-18 12:23:23.531');
INSERT INTO `ct_sys_role_perm` VALUES (1, 48, '', '', '2019-12-18 12:23:23.531', '2019-12-18 12:23:23.531');
INSERT INTO `ct_sys_role_perm` VALUES (1, 49, '', '', '2019-12-18 12:23:23.531', '2019-12-18 12:23:23.531');
INSERT INTO `ct_sys_role_perm` VALUES (1, 50, '', '', '2019-12-18 12:23:23.531', '2019-12-18 12:23:23.531');
INSERT INTO `ct_sys_role_perm` VALUES (1, 53, '', '', '2019-12-18 12:23:23.531', '2019-12-18 12:23:23.531');
INSERT INTO `ct_sys_role_perm` VALUES (1, 56, '', '', '2019-12-18 12:23:23.531', '2019-12-18 12:23:23.531');
INSERT INTO `ct_sys_role_perm` VALUES (1, 57, '', '', '2019-12-18 12:23:23.531', '2019-12-18 12:23:23.531');
INSERT INTO `ct_sys_role_perm` VALUES (1, 58, '', '', '2019-12-18 12:23:23.531', '2019-12-18 12:23:23.531');
INSERT INTO `ct_sys_role_perm` VALUES (1, 59, '', '', '2019-12-18 12:23:23.531', '2019-12-18 12:23:23.531');
INSERT INTO `ct_sys_role_perm` VALUES (1, 60, '', '', '2019-12-18 12:23:23.531', '2019-12-18 12:23:23.531');
INSERT INTO `ct_sys_role_perm` VALUES (1, 61, '', '', '2019-12-18 12:23:23.531', '2019-12-18 12:23:23.531');
INSERT INTO `ct_sys_role_perm` VALUES (1, 62, '', '', '2019-12-18 12:23:23.531', '2019-12-18 12:23:23.531');
INSERT INTO `ct_sys_role_perm` VALUES (1, 64, '', '', '2019-12-18 12:23:23.531', '2019-12-18 12:23:23.531');
INSERT INTO `ct_sys_role_perm` VALUES (1, 65, '', '', '2019-12-18 12:23:23.531', '2019-12-18 12:23:23.531');
INSERT INTO `ct_sys_role_perm` VALUES (1, 66, '', '', '2019-12-18 12:23:23.531', '2019-12-18 12:23:23.531');
INSERT INTO `ct_sys_role_perm` VALUES (1, 67, '', '', '2019-12-18 12:23:23.531', '2019-12-18 12:23:23.531');
INSERT INTO `ct_sys_role_perm` VALUES (1, 68, '', '', '2019-12-18 12:23:23.531', '2019-12-18 12:23:23.531');
INSERT INTO `ct_sys_role_perm` VALUES (1, 69, '', '', '2019-12-18 12:23:23.531', '2019-12-18 12:23:23.531');
INSERT INTO `ct_sys_role_perm` VALUES (1, 70, '', '', '2019-12-18 12:23:23.531', '2019-12-18 12:23:23.531');
INSERT INTO `ct_sys_role_perm` VALUES (1, 71, '', '', '2019-12-18 12:23:23.531', '2019-12-18 12:23:23.531');
INSERT INTO `ct_sys_role_perm` VALUES (1, 72, '', '', '2019-12-18 12:23:23.531', '2019-12-18 12:23:23.531');
INSERT INTO `ct_sys_role_perm` VALUES (1, 73, '', '', '2019-12-18 12:23:23.531', '2019-12-18 12:23:23.531');
INSERT INTO `ct_sys_role_perm` VALUES (1, 74, '', '', '2019-12-18 12:23:23.531', '2019-12-18 12:23:23.531');
INSERT INTO `ct_sys_role_perm` VALUES (1, 75, '', '', '2019-12-18 12:23:23.531', '2019-12-18 12:23:23.531');
INSERT INTO `ct_sys_role_perm` VALUES (1, 76, '', '', '2019-12-18 12:23:23.531', '2019-12-18 12:23:23.531');
INSERT INTO `ct_sys_role_perm` VALUES (1, 78, '', '', '2019-12-18 12:23:23.531', '2019-12-18 12:23:23.531');
INSERT INTO `ct_sys_role_perm` VALUES (1, 80, '', '', '2019-12-18 12:23:23.531', '2019-12-18 12:23:23.531');
INSERT INTO `ct_sys_role_perm` VALUES (1, 81, '', '', '2019-12-18 12:23:23.531', '2019-12-18 12:23:23.531');
INSERT INTO `ct_sys_role_perm` VALUES (1, 82, '', '', '2019-12-18 12:23:23.531', '2019-12-18 12:23:23.531');
INSERT INTO `ct_sys_role_perm` VALUES (1, 83, '', '', '2019-12-18 12:23:23.531', '2019-12-18 12:23:23.531');
INSERT INTO `ct_sys_role_perm` VALUES (1, 84, '', '', '2019-12-18 12:23:23.531', '2019-12-18 12:23:23.531');
INSERT INTO `ct_sys_role_perm` VALUES (1, 85, '', '', '2019-12-18 12:23:23.531', '2019-12-18 12:23:23.531');
INSERT INTO `ct_sys_role_perm` VALUES (1, 86, '', '', '2019-12-18 12:23:23.531', '2019-12-18 12:23:23.531');
INSERT INTO `ct_sys_role_perm` VALUES (1, 87, '', '', '2019-12-18 12:23:23.531', '2019-12-18 12:23:23.531');
INSERT INTO `ct_sys_role_perm` VALUES (1, 88, '', '', '2019-12-18 12:23:23.531', '2019-12-18 12:23:23.531');

-- ----------------------------
-- Table structure for ct_sys_user_admin
-- ----------------------------
DROP TABLE IF EXISTS `ct_sys_user_admin`;
CREATE TABLE `ct_sys_user_admin`  (
  `USER_ID` int(11) UNSIGNED NOT NULL COMMENT '用户ID',
  PRIMARY KEY (`USER_ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '系统管理员账号-名单表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of ct_sys_user_admin
-- ----------------------------
INSERT INTO `ct_sys_user_admin` VALUES (1);

-- ----------------------------
-- Table structure for ct_sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `ct_sys_user_role`;
CREATE TABLE `ct_sys_user_role`  (
  `USER_ID` int(11) UNSIGNED NOT NULL COMMENT '用户ID',
  `ROLE_ID` int(11) UNSIGNED NOT NULL COMMENT '角色ID',
  `OPERATOR` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '操作者',
  `OPERATOR_IP` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '最后一次更新者的ip地址',
  `CREATE_TIME` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) COMMENT '添加时间',
  `UPDATE_TIME` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3) COMMENT '最后更新时间',
  PRIMARY KEY (`USER_ID`, `ROLE_ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '系统用户-角色关联表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of ct_sys_user_role
-- ----------------------------
INSERT INTO `ct_sys_user_role` VALUES (1, 1, 'admin', '0.0.0.0', '2019-12-18 12:23:23.677', '2019-12-18 12:23:23.677');

-- ----------------------------
-- Table structure for t_sys_access_log
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_access_log`;
CREATE TABLE `t_sys_access_log`  (
  `ID` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `USER_ID` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '用户id,有则记录，没有为0',
  `USER_NAME` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '访问的用户名，没有则为空字符串',
  `URL_NAME` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '访问的URL名称',
  `IP` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '用户访问的ip地址',
  `PORT` int(10) UNSIGNED NOT NULL COMMENT '访问端口',
  `URL` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '访问的URL路径',
  `ADDRESS` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'ip定位地址',
  `MAC` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '' COMMENT '客户端的MAC地址，此数据不成立，所有查看到的实现方式都是错误的，过时的。不是真正的mac地址',
  `PARAMS` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL COMMENT '访问参数',
  `HTTP_METHOD` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '接口调用类型',
  `OS` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '操作系统',
  `RENDER_ENGINE` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '' COMMENT '渲染引擎标志',
  `BROWSER_VERSION` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '' COMMENT '浏览器版本',
  `BROWSER_TYPE` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '' COMMENT '访问设备类型',
  `BROWSER_NAME` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '' COMMENT '访问设备名称',
  `CREATE_TIME` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) COMMENT '记录时间',
  PRIMARY KEY (`ID`) USING BTREE,
  INDEX `USER_ID`(`USER_ID`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '访问日志记录表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for t_sys_banner
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_banner`;
CREATE TABLE `t_sys_banner`  (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `TITLE` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '标题',
  `IMAGE_PATH` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '高清图片',
  `THUMB_NAIL_IMAGE_PATH` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '缩略图片',
  `URL` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '跳转链接',
  `SKIP_WAY` int(2) NOT NULL DEFAULT 0 COMMENT '跳转方式：0不跳转。1跳转',
  `TYPE` int(2) NOT NULL DEFAULT 1 COMMENT 'banner类型：1为app端，2为电脑端',
  `STATE` int(2) NOT NULL DEFAULT 0 COMMENT '状态：0待审核，1已审核,2已禁用',
  `OPERATOR` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '操作者',
  `OPERATOR_IP` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '最后一次更新者的ip地址',
  `CREATE_TIME` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) COMMENT '第一次添加时间',
  `UPDATE_TIME` datetime(3) NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3) COMMENT '最后更新时间',
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '系统banner设置表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for t_sys_config
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_config`;
CREATE TABLE `t_sys_config`  (
  `ID` int(4) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `KEY_NAME` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'key名称',
  `DESCRIPT` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'key的中文描述信息',
  `KEY_VALUE` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'key的值',
  `OPERATOR` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '操作者用户名',
  `OPERATOR_IP` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '最后一次更新者的ip地址',
  `UPDATE_TIME` datetime(3) NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3) COMMENT '最后更新时间',
  `CREATE_TIME` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) COMMENT '第一次添加时间',
  PRIMARY KEY (`ID`, `OPERATOR`) USING BTREE,
  UNIQUE INDEX `KEY_NAME`(`KEY_NAME`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 13 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '系统设置表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of t_sys_config
-- ----------------------------
INSERT INTO `t_sys_config` VALUES (1, 'webName', '网站名称（显示于客户端,后台登陆、管理界面）', '告诉你', 'admin', '0:0:0:0:0:0:0:1', '2019-08-16 09:57:00.320', '2019-01-18 18:00:04.202');
INSERT INTO `t_sys_config` VALUES (2, 'serviceLocked', '网站会员访问锁定(1代表锁定，其他不锁定)', '0', 'admin', '127.0.0.1', '2019-02-19 12:03:19.897', '2019-01-18 23:52:45.809');
INSERT INTO `t_sys_config` VALUES (3, 'customServiceLink', '客服链接', '', 'admin', '115.84.98.46', '2019-07-17 09:13:36.429', '2019-01-19 00:30:15.893');
INSERT INTO `t_sys_config` VALUES (4, 'friendlyReminder', '温馨提示', '', 'admin', '117.20.115.131', '2019-07-17 09:13:45.021', '2019-01-20 23:43:16.252');
INSERT INTO `t_sys_config` VALUES (5, 'companyName', '公司标志', 'Copyright @2019-2020 告诉你. All Rights Reserved', 'admin', '0:0:0:0:0:0:0:1', '2019-08-16 09:56:23.646', '2019-01-20 23:44:06.416');
INSERT INTO `t_sys_config` VALUES (6, 'serviceLockedInfo', '网站访问锁定提示信息', '服务器升级，暂停访问', 'admin', '117.20.115.131', '2019-07-17 09:15:17.988', '2019-01-22 11:49:12.959');
INSERT INTO `t_sys_config` VALUES (7, 'clientBgImage', '客户端背景图', '/static/image/otherImage/20190715/8b2d7237c1f14d4f8b3708085a0f8749.jpg', 'admin', '0:0:0:0:0:0:0:1', '2019-07-17 09:10:09.184', '2019-02-18 10:24:56.348');
INSERT INTO `t_sys_config` VALUES (8, 'noticeBarId', '首页通告栏的资讯id（通告栏滚动只显示标题，但是可以打开内容详情）', '', 'admin', '27.109.113.247', '2019-08-05 17:37:12.262', '2019-02-25 17:06:31.704');
INSERT INTO `t_sys_config` VALUES (9, 'webLogo', '网站logo图标', '/static/image/otherImage/20190815/3364be00456f4a7c9ec5f01f9ab3377a.png', 'admin', '0:0:0:0:0:0:0:1', '2019-07-17 09:10:12.868', '2019-03-04 11:03:49.793');
INSERT INTO `t_sys_config` VALUES (10, 'clientBgImagePC', '网站PC客户端端背景图', '/static/image/otherImage/20190815/12421be93c8b4597afd27fc934f773ec.jpg', 'admin', '0:0:0:0:0:0:0:1', '2019-07-17 09:10:14.468', '2019-05-05 17:36:11.831');
INSERT INTO `t_sys_config` VALUES (11, 'checkInReward', '每日签到积分奖励', '50', 'admin', '0:0:0:0:0:0:0:1', '2019-07-21 22:41:52.980', '2019-07-21 22:41:49.357');
INSERT INTO `t_sys_config` VALUES (12, 'checkInCount', '每日可签到次数', '1', 'admin', '0:0:0:0:0:0:0:1', '2019-07-21 22:42:25.390', '2019-07-21 22:42:25.390');

-- ----------------------------
-- Table structure for t_sys_information
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_information`;
CREATE TABLE `t_sys_information`  (
  `ID` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `AUTHOR` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '作者',
  `TITLE` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '作者',
  `STATE` int(2) NOT NULL DEFAULT 0 COMMENT '状态：0待审核，1已审核,2已禁用',
  `CONTENT` mediumtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '文章内容',
  `DESCRIPT` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '简介',
  `OPERATOR` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '操作者',
  `OPERATOR_IP` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '最后一次更新者的ip地址',
  `CREATE_TIME` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) COMMENT '第一次添加时间',
  `UPDATE_TIME` datetime(3) NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3) COMMENT '最后更新时间',
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '系统资讯表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for t_sys_member_asset
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_member_asset`;
CREATE TABLE `t_sys_member_asset`  (
  `ID` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '用户主键id',
  `BALANCE` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '用户余额',
  `FREEZE_BALANCE` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '账户余额冻结',
  `CREATE_TIME` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) COMMENT '第一次添加时间',
  `UPDATE_TIME` datetime(3) NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3) COMMENT '最后更新时间',
  PRIMARY KEY (`ID`) USING BTREE,
  INDEX `UPDATE_TIME`(`UPDATE_TIME`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '会员资产表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of t_sys_member_asset
-- ----------------------------
INSERT INTO `t_sys_member_asset` VALUES (1, 50, 0, '2019-12-18 12:23:23.473', '2019-12-18 12:23:52.065');

-- ----------------------------
-- Table structure for t_sys_member_asset_log
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_member_asset_log`;
CREATE TABLE `t_sys_member_asset_log`  (
  `ID` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `USER_ID` int(11) UNSIGNED NOT NULL COMMENT '用户主键id',
  `SERIAL_NO` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '账变记录对应的流水号,后续可以根据账变类型去查相应的触发记录',
  `TYPE` int(2) UNSIGNED NOT NULL COMMENT '变更类型：0、新建记录',
  `BALANCE` int(11) NOT NULL COMMENT '产生的余额数值：增加为正整数。减少为负数',
  `ASSET_BALANCE` int(11) NOT NULL COMMENT '变更后的余额数值',
  `ASSET_FREEZE_BALANCE` int(11) NOT NULL COMMENT '变更后的冻结余额数值',
  `DESCRIPT` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '账变描述信息',
  `OPERATOR` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '操作者用户名',
  `OPERATOR_IP` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '最后一次更新者的ip地址',
  `CREATE_TIME` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) COMMENT '第一次添加时间',
  `UPDATE_TIME` datetime(3) NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3) COMMENT '最后更新时间',
  PRIMARY KEY (`ID`) USING BTREE,
  INDEX `USER_ID`(`USER_ID`, `TYPE`, `SERIAL_NO`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '会员资产变更记录表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of t_sys_member_asset_log
-- ----------------------------
INSERT INTO `t_sys_member_asset_log` VALUES (1, 1, '1912188TPZRWH568', 1, 50, 50, 0, '签到奖励', 'admin', '0.0.0.0', '2019-12-18 12:23:52.126', '2019-12-18 12:23:52.126');

-- ----------------------------
-- Table structure for t_sys_member_check_in
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_member_check_in`;
CREATE TABLE `t_sys_member_check_in`  (
  `ID` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `USER_ID` int(11) NOT NULL COMMENT '用户id',
  `USER_NAME` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '用户名',
  `CHECK_IN_DATE` date NOT NULL COMMENT '签到日期',
  `CREATE_TIME` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) COMMENT '添加时间',
  `UPDATE_TIME` datetime(3) NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3) COMMENT '最后更新时间',
  PRIMARY KEY (`ID`) USING BTREE,
  UNIQUE INDEX `USER_ID`(`USER_ID`, `CHECK_IN_DATE`) USING BTREE,
  INDEX `CHECK_IN_DATE`(`CHECK_IN_DATE`) USING BTREE,
  INDEX `CREATE_TIME`(`CREATE_TIME`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '系统会员签到表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of t_sys_member_check_in
-- ----------------------------
INSERT INTO `t_sys_member_check_in` VALUES (1, 1, 'admin', '2019-12-18', '2019-12-18 12:23:52.070', '2019-12-18 12:23:52.070');

-- ----------------------------
-- Table structure for t_sys_member_check_in_reward
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_member_check_in_reward`;
CREATE TABLE `t_sys_member_check_in_reward`  (
  `ID` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `CHECK_IN_ID` int(11) UNSIGNED NOT NULL COMMENT '签到id',
  `CHECK_IN_REWARD_BALANCE` int(11) NOT NULL COMMENT '签到奖励金额',
  `STATE` tinyint(1) NOT NULL DEFAULT 0 COMMENT '奖励发放状态:0,待发放，1已发放，2，已拒绝',
  `OPERATOR` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '操作人',
  `OPERATOR_IP` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '操作ip',
  `CREATE_TIME` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) COMMENT '添加时间',
  `UPDATE_TIME` datetime(3) NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3) COMMENT '最后更新时间',
  PRIMARY KEY (`ID`) USING BTREE,
  INDEX `CHECK_IN_ID`(`CHECK_IN_ID`) USING BTREE,
  INDEX `CHECK_IN_REWARD_BALANCE`(`CHECK_IN_REWARD_BALANCE`) USING BTREE,
  INDEX `STATE`(`STATE`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '系统会员签到奖励详情表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of t_sys_member_check_in_reward
-- ----------------------------
INSERT INTO `t_sys_member_check_in_reward` VALUES (1, 1, 50, 1, 'admin', '0.0.0.0', '2019-12-18 12:23:52.075', '2019-12-18 12:23:52.075');

-- ----------------------------
-- Table structure for t_sys_member_pwd_reset
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_member_pwd_reset`;
CREATE TABLE `t_sys_member_pwd_reset`  (
  `ID` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '表主键',
  `USER_NAME` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '用户名',
  `PASSWORD` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '密码',
  `REASON` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '申请原因',
  `STATE` tinyint(4) UNSIGNED NOT NULL DEFAULT 0 COMMENT '申请状态：0为待处理,1已处理,2已拒绝',
  `DESCRIPT` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '处理结果',
  `OPERATOR` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '操作者',
  `OPERATOR_IP` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '最后一次更新者的ip地址',
  `CREATE_TIME` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) COMMENT '第一次添加时间',
  `UPDATE_TIME` datetime(3) NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3) COMMENT '最后更新时间',
  PRIMARY KEY (`ID`) USING BTREE,
  INDEX `USER_NAME`(`USER_NAME`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '密码重置申请表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for t_sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_menu`;
CREATE TABLE `t_sys_menu`  (
  `ID` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `NAME` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '菜单名称',
  `PARENT` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '父菜单id',
  `DESC` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '备注',
  `TYPE` int(11) UNSIGNED NOT NULL DEFAULT 1 COMMENT '菜单类型：1为根菜单，2为子菜单',
  `GROUP` tinyint(4) NOT NULL DEFAULT 0 COMMENT '分组',
  `SORT` int(4) NOT NULL DEFAULT 0 COMMENT '排序',
  `PATH` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '菜单路径',
  `OPEN` tinyint(4) NOT NULL DEFAULT 0 COMMENT '菜单是否为公开',
  `OPERATOR` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '操作者',
  `OPERATOR_IP` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '最后一次更新者的ip地址',
  `CREATE_TIME` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) COMMENT '添加时间',
  `UPDATE_TIME` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3) COMMENT '最后更新时间',
  PRIMARY KEY (`ID`) USING BTREE,
  INDEX `PARENT`(`PARENT`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 61 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '系统菜单表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of t_sys_menu
-- ----------------------------
INSERT INTO `t_sys_menu` VALUES (1, '用户管理', 0, '用户管理菜单', 1, 1, 1, '', 0, '', '', '2018-10-25 16:15:51.998', '2018-10-25 16:15:51.998');
INSERT INTO `t_sys_menu` VALUES (2, '管理员', 1, '平台管理人员账号', 2, 1, 2, '/sys/users/findAll', 0, '', '', '2018-10-25 16:16:24.331', '2019-01-11 09:05:47.373');
INSERT INTO `t_sys_menu` VALUES (3, '系统管理', 0, '系统管理菜单', 1, 2, 6, '', 0, '', '', '2018-10-25 16:16:58.224', '2019-03-16 02:45:30.902');
INSERT INTO `t_sys_menu` VALUES (5, '权限列表', 3, '查看所有权限列表', 2, 3, 2, '/sys/perms/findAll', 0, '', '', '2018-10-25 16:18:22.611', '2019-07-17 08:45:10.888');
INSERT INTO `t_sys_menu` VALUES (7, '菜单列表', 3, '系统菜单列表', 2, 3, 4, '/sys/menus/findAll', 0, '', '', '2018-10-25 16:19:19.863', '2019-07-17 08:48:49.415');
INSERT INTO `t_sys_menu` VALUES (9, '角色列表', 3, '角色列表菜单', 2, 4, 1, '/sys/roles/findAll', 0, '', '', '2018-10-25 16:20:49.769', '2019-07-17 08:45:40.127');
INSERT INTO `t_sys_menu` VALUES (11, '定时任务', 3, '定时任务列表', 2, 5, 2, '/sys/task/findAll', 0, '', '', '2018-11-23 12:46:24.694', '2019-07-17 08:45:48.399');
INSERT INTO `t_sys_menu` VALUES (19, '注册会员', 1, '注册用户列表', 2, 1, 1, '/sys/members/findAll', 0, '', '', '2019-01-11 09:02:45.397', '2019-01-11 09:06:17.179');
INSERT INTO `t_sys_menu` VALUES (20, '初始化配置', 3, '系统初始化配置', 2, 4, 5, '/sys/system/findAll', 0, '', '', '2019-01-18 17:20:04.950', '2019-07-17 08:44:41.780');
INSERT INTO `t_sys_menu` VALUES (23, '会员功能', 0, '会员功能菜单', 1, 1, 2, '', 0, '', '', '2019-02-01 23:50:16.281', '2019-03-16 02:45:09.982');
INSERT INTO `t_sys_menu` VALUES (32, '客户端设置', 0, '客户端设置', 1, 6, 7, '', 0, '', '', '2019-02-18 10:09:40.336', '2019-03-16 02:45:34.273');
INSERT INTO `t_sys_menu` VALUES (33, '背景图更新', 32, '背景图更新', 2, 1, 1, '/sys/client/bgImg', 0, '', '', '2019-02-18 10:10:13.275', '2019-02-18 10:10:13.275');
INSERT INTO `t_sys_menu` VALUES (34, '登陆密码重置', 23, '用户登陆密码重置', 2, 4, 7, '/sys/member/pwdResetApply', 0, '', '', '2019-02-19 09:07:09.613', '2019-07-22 10:12:51.310');
INSERT INTO `t_sys_menu` VALUES (37, '轮播图管理', 32, '轮播图列表', 2, 1, 2, '/sys/client/banner', 0, '', '', '2019-02-21 16:27:42.617', '2019-02-21 16:27:42.617');
INSERT INTO `t_sys_menu` VALUES (38, '资讯管理', 0, '资讯公告管理', 1, 1, 8, '', 0, '', '', '2019-02-21 16:30:22.262', '2019-03-16 02:45:39.118');
INSERT INTO `t_sys_menu` VALUES (39, '资讯列表', 38, '系统资讯列表', 2, 1, 1, '/sys/information/findAll', 0, '', '', '2019-02-21 16:31:15.668', '2019-02-21 16:31:15.668');
INSERT INTO `t_sys_menu` VALUES (40, '网站LOGO', 32, '更新客户端logo', 2, 1, 3, '/sys/client/webLogo', 0, '', '', '2019-03-04 23:28:10.982', '2019-03-04 23:29:07.124');
INSERT INTO `t_sys_menu` VALUES (44, '日志管理', 0, '日志管理菜单', 1, 1, 9, '', 0, '', '', '2019-03-15 13:14:05.155', '2019-03-16 02:46:22.270');
INSERT INTO `t_sys_menu` VALUES (45, '用户访问日志', 44, '访问日志', 2, 1, 1, '/sys/log/findAllAccessLog', 0, '', '', '2019-03-15 13:14:57.709', '2019-03-15 13:14:57.709');
INSERT INTO `t_sys_menu` VALUES (46, '系统日志文件', 44, '本地日志文件列表', 2, 1, 2, '/sys/log/getAllSysLogFile', 0, '', '', '2019-05-22 09:02:38.307', '2019-05-22 09:02:38.307');
INSERT INTO `t_sys_menu` VALUES (56, '积分管理', 0, '积分管理菜单', 1, 1, 1, '', 0, '', '', '2019-07-22 10:12:32.454', '2019-07-22 10:12:32.454');
INSERT INTO `t_sys_menu` VALUES (57, '会员积分', 56, '会员账户积分', 2, 1, 1, '/sys/member/asset', 0, '', '', '2019-07-22 10:13:15.954', '2019-07-22 10:13:15.954');
INSERT INTO `t_sys_menu` VALUES (58, '积分记录', 56, '积分使用记录', 2, 1, 2, '/sys/member/assetChangeLog', 0, '', '', '2019-07-22 10:13:30.636', '2019-07-22 10:13:30.636');
INSERT INTO `t_sys_menu` VALUES (60, '签到记录', 23, '会员签到记录', 2, 1, 2, '/sys/member/checkInLog', 0, '', '', '2019-07-23 11:41:40.587', '2019-07-23 11:42:42.054');

-- ----------------------------
-- Table structure for t_sys_perm
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_perm`;
CREATE TABLE `t_sys_perm`  (
  `ID` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '权限ID主键',
  `NAME` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '权限名称',
  `DESC` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '' COMMENT '权限描述',
  `HTTP_METHOD` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'GET' COMMENT 'Http权限请求类型',
  `TYPE` tinyint(4) UNSIGNED NOT NULL COMMENT '权限类型 1、菜单2、按钮、3其他',
  `GROUP` tinyint(4) UNSIGNED NOT NULL COMMENT '所属的权限组：',
  `URL` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '系统权限请求路径',
  `SORT` int(4) UNSIGNED NOT NULL DEFAULT 0 COMMENT '权限在当前模块下的顺序',
  `CLASS_NAME` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '接口所在的系统包名',
  `METHOD_NAME` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '类方法名称',
  `OPEN` tinyint(4) UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否是公开权限',
  `STATE` tinyint(3) UNSIGNED NOT NULL DEFAULT 2 COMMENT '状态:默认为1,0为禁用,2,为登陆可用',
  `OPERATOR` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '操作者',
  `OPERATOR_IP` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '最后一次更新者的ip地址',
  `CREATE_TIME` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) COMMENT '添加时间',
  `UPDATE_TIME` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3) COMMENT '最后更新时间',
  PRIMARY KEY (`ID`) USING BTREE,
  UNIQUE INDEX `NAME`(`NAME`) USING BTREE,
  UNIQUE INDEX `URL`(`URL`) USING BTREE,
  INDEX `GROUP`(`GROUP`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 90 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '系统权限表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of t_sys_perm
-- ----------------------------
INSERT INTO `t_sys_perm` VALUES (1, '查询所有轮播广告', '客户端设置组', 'GET', 2, 13, '/sys/banner/findAllSysBanner', 3, 'com.jtw.sys.controller.banner.SysBannerController', 'findAllSysBanner', 0, 1, 'admin', '0.0.0.0', '2019-12-18 12:23:23.308', '2019-12-18 12:23:23.308');
INSERT INTO `t_sys_perm` VALUES (2, '添加轮播广告', '客户端设置组', 'POST', 2, 13, '/sys/banner/addSysBanner', 1, 'com.jtw.sys.controller.banner.SysBannerController', 'addSysBanner', 0, 1, 'admin', '0.0.0.0', '2019-12-18 12:23:23.308', '2019-12-18 12:23:23.308');
INSERT INTO `t_sys_perm` VALUES (3, '修改轮播广告', '客户端设置组', 'POST', 2, 13, '/sys/banner/modifySysBanner', 2, 'com.jtw.sys.controller.banner.SysBannerController', 'modifySysBanner', 0, 1, 'admin', '0.0.0.0', '2019-12-18 12:23:23.308', '2019-12-18 12:23:23.308');
INSERT INTO `t_sys_perm` VALUES (4, '新建客服房间', '客服管理组', 'POST', 2, 14, '/sys/im/addService', 1, 'com.jtw.sys.controller.im.imController', 'addService', 0, 1, 'admin', '0.0.0.0', '2019-12-18 12:23:23.308', '2019-12-18 12:23:23.308');
INSERT INTO `t_sys_perm` VALUES (5, '查询系统访问日志', '系统登陆可用组', 'GET', 2, 99, '/sys/log/findAllAccessLog', 9, 'com.jtw.sys.controller.system.LogController', 'findAll', 0, 1, 'admin', '0.0.0.0', '2019-12-18 12:23:23.308', '2019-12-18 12:23:23.308');
INSERT INTO `t_sys_perm` VALUES (6, '清除系统访问日志', '系统登陆可用组', 'POST', 2, 99, '/sys/log/delAccessLogBeforeDay', 10, 'com.jtw.sys.controller.system.LogController', 'delAccessLogBeforeDay', 0, 1, 'admin', '0.0.0.0', '2019-12-18 12:23:23.309', '2019-12-18 12:23:23.309');
INSERT INTO `t_sys_perm` VALUES (7, '删除单个系统角色', '系统角色管理', 'POST', 2, 4, '/sys/role/delRole', 4, 'com.jtw.sys.controller.user.SysRoleController', 'delRole', 0, 1, 'admin', '0.0.0.0', '2019-12-18 12:23:23.309', '2019-12-18 12:23:23.309');
INSERT INTO `t_sys_perm` VALUES (8, '添加单个系统角色', '系统角色管理', 'POST', 2, 4, '/sys/role/addRole', 3, 'com.jtw.sys.controller.user.SysRoleController', 'addRole', 0, 1, 'admin', '0.0.0.0', '2019-12-18 12:23:23.309', '2019-12-18 12:23:23.309');
INSERT INTO `t_sys_perm` VALUES (9, '查询所有系统角色', '系统角色管理', 'GET', 2, 4, '/sys/role/findAllRoles', 1, 'com.jtw.sys.controller.user.SysRoleController', 'findAllSysRoles', 0, 1, 'admin', '0.0.0.0', '2019-12-18 12:23:23.309', '2019-12-18 12:23:23.309');
INSERT INTO `t_sys_perm` VALUES (10, '修改单个系统角色', '系统角色管理', 'POST', 2, 4, '/sys/role/modifyRole', 2, 'com.jtw.sys.controller.user.SysRoleController', 'modifyRole', 0, 1, 'admin', '0.0.0.0', '2019-12-18 12:23:23.309', '2019-12-18 12:23:23.309');
INSERT INTO `t_sys_perm` VALUES (11, '修改单个角色权限', '系统权限管理', 'POST', 2, 3, '/sys/perm/modifyRolePerms', 2, 'com.jtw.sys.controller.perm.SysPermController', 'modifyRolePerms', 0, 1, 'admin', '0.0.0.0', '2019-12-18 12:23:23.309', '2019-12-18 12:23:23.309');
INSERT INTO `t_sys_perm` VALUES (12, '修改单条权限基本信息', '系统权限管理', 'POST', 2, 3, '/sys/perm/modifySysPerm', 7, 'com.jtw.sys.controller.perm.SysPermController', 'modifySysPerm', 0, 1, 'admin', '0.0.0.0', '2019-12-18 12:23:23.310', '2019-12-18 12:23:23.310');
INSERT INTO `t_sys_perm` VALUES (13, '添加单个系统权限', '系统权限管理', 'POST', 2, 3, '/sys/perm/addSysPerm', 1, 'com.jtw.sys.controller.perm.SysPermController', 'addSysPerm', 0, 1, 'admin', '0.0.0.0', '2019-12-18 12:23:23.310', '2019-12-18 12:23:23.310');
INSERT INTO `t_sys_perm` VALUES (14, '查询所有系统权限', '系统权限管理', 'GET', 2, 3, '/sys/perm/findAllSysPerms', 6, 'com.jtw.sys.controller.perm.SysPermController', 'findAllSysPerms', 0, 1, 'admin', '0.0.0.0', '2019-12-18 12:23:23.310', '2019-12-18 12:23:23.310');
INSERT INTO `t_sys_perm` VALUES (15, '获取单个角色所有权限', '系统权限管理', 'GET', 2, 3, '/sys/perm/getAllRolePermsByRoleId', 5, 'com.jtw.sys.controller.perm.SysPermController', 'getAllRolePermsByRoleId', 0, 1, 'admin', '0.0.0.0', '2019-12-18 12:23:23.310', '2019-12-18 12:23:23.310');
INSERT INTO `t_sys_perm` VALUES (16, '获取单个角色未拥有的权限', '系统权限管理', 'GET', 2, 3, '/sys/perm/getAllRolePermsByRoleIdNotHave', 3, 'com.jtw.sys.controller.perm.SysPermController', 'getAllRolePermsByRoleIdNotHave', 0, 1, 'admin', '0.0.0.0', '2019-12-18 12:23:23.310', '2019-12-18 12:23:23.310');
INSERT INTO `t_sys_perm` VALUES (17, '获取单个角色的权限树并标记', '系统权限管理', 'GET', 2, 3, '/sys/perm/getAllSysPermTreeMarkByRoleId', 4, 'com.jtw.sys.controller.perm.SysPermController', 'getAllSysPermTreeMarkByRoleId', 0, 1, 'admin', '0.0.0.0', '2019-12-18 12:23:23.310', '2019-12-18 12:23:23.310');
INSERT INTO `t_sys_perm` VALUES (18, '系统用户登录入口', '公开权限组', 'POST', 2, 1, '/sys/user/login', 1, 'com.jtw.sys.controller.user.SysUserController', 'login', 1, 1, 'admin', '0.0.0.0', '2019-12-18 12:23:23.311', '2019-12-18 12:23:23.311');
INSERT INTO `t_sys_perm` VALUES (19, '系统用户退出登录', '系统登陆可用组', 'POST', 2, 99, '/sys/user/logout', 1, 'com.jtw.sys.controller.user.SysUserController', 'logout', 0, 1, 'admin', '0.0.0.0', '2019-12-18 12:23:23.311', '2019-12-18 12:23:23.311');
INSERT INTO `t_sys_perm` VALUES (20, '无权限操作提示', '公开权限组', 'GET', 2, 1, '/sys/user/noPermission', 3, 'com.jtw.sys.controller.user.SysUserController', 'noPermission', 1, 1, 'admin', '0.0.0.0', '2019-12-18 12:23:23.311', '2019-12-18 12:23:23.311');
INSERT INTO `t_sys_perm` VALUES (21, '未登录提示接口', '公开权限组', 'GET', 2, 1, '/sys/user/unLogin', 2, 'com.jtw.sys.controller.user.SysUserController', 'unLogin', 1, 1, 'admin', '0.0.0.0', '2019-12-18 12:23:23.311', '2019-12-18 12:23:23.311');
INSERT INTO `t_sys_perm` VALUES (22, '添加管理账号', '系统用户管理', 'POST', 2, 5, '/sys/user/addSysUserAccount', 2, 'com.jtw.sys.controller.user.SysUserController', 'addSysUserAccount', 0, 1, 'admin', '0.0.0.0', '2019-12-18 12:23:23.311', '2019-12-18 12:23:23.311');
INSERT INTO `t_sys_perm` VALUES (23, '修改单个用户基本信息', '系统用户基本信息管理', 'POST', 2, 5, '/sys/user/modifySysUserBaseInfoByUserId', 6, 'com.jtw.sys.controller.user.SysUserController', 'modifySysUserBaseInfoByUserId', 0, 1, 'admin', '0.0.0.0', '2019-12-18 12:23:23.311', '2019-12-18 12:23:23.311');
INSERT INTO `t_sys_perm` VALUES (24, '获取当前管理员的基本信息', '系统登陆可用组', 'GET', 2, 99, '/sys/user/getSysUserBaseInfoVo', 3, 'com.jtw.sys.controller.user.SysUserController', 'getSysUserBaseInfoVo', 0, 1, 'admin', '0.0.0.0', '2019-12-18 12:23:23.311', '2019-12-18 12:23:23.311');
INSERT INTO `t_sys_perm` VALUES (25, '解锁用户账号', '系统用户管理', 'POST', 2, 5, '/sys/user/unlockSysUserAccount', 4, 'com.jtw.sys.controller.user.SysUserController', 'unlockSysUserAccount', 0, 1, 'admin', '0.0.0.0', '2019-12-18 12:23:23.312', '2019-12-18 12:23:23.312');
INSERT INTO `t_sys_perm` VALUES (26, '修改单个用户角色', '系统用户角色管理', 'POST', 2, 5, '/sys/user/modifyUserRolesByUserId', 5, 'com.jtw.sys.controller.user.SysUserController', 'modifyUserRolesByUserId', 0, 1, 'admin', '0.0.0.0', '2019-12-18 12:23:23.312', '2019-12-18 12:23:23.312');
INSERT INTO `t_sys_perm` VALUES (27, '锁定用户账号', '系统用户管理', 'POST', 2, 5, '/sys/user/lockSysUserAccount', 3, 'com.jtw.sys.controller.user.SysUserController', 'lockSysUserAccount', 0, 1, 'admin', '0.0.0.0', '2019-12-18 12:23:23.312', '2019-12-18 12:23:23.312');
INSERT INTO `t_sys_perm` VALUES (28, '获取单个用户所有权限', '系统用户权限管理', 'GET', 2, 5, '/sys/user/getAllPermsBySysUserId', 7, 'com.jtw.sys.controller.user.SysUserController', 'getAllPermsBySysUserId', 0, 1, 'admin', '0.0.0.0', '2019-12-18 12:23:23.312', '2019-12-18 12:23:23.312');
INSERT INTO `t_sys_perm` VALUES (29, '查询所有后台管理员用户', '系统用户管理', 'GET', 2, 5, '/sys/user/findAllAdminUserInfoBaseVo', 12, 'com.jtw.sys.controller.user.SysUserController', 'findAllAdminUserInfoBaseVo', 0, 1, 'admin', '0.0.0.0', '2019-12-18 12:23:23.313', '2019-12-18 12:23:23.313');
INSERT INTO `t_sys_perm` VALUES (30, '获取单个用户的角色', '系统用户角色管理', 'GET', 2, 5, '/sys/user/getUserRolesByUserId', 7, 'com.jtw.sys.controller.user.SysUserController', 'getUserRolesByUserId', 0, 1, 'admin', '0.0.0.0', '2019-12-18 12:23:23.313', '2019-12-18 12:23:23.313');
INSERT INTO `t_sys_perm` VALUES (31, '查询所有系统用户', '系统用户管理', 'GET', 2, 5, '/sys/user/findAllUserInfoBaseVo', 1, 'com.jtw.sys.controller.user.SysUserController', 'findAllUserInfoBaseVo', 0, 1, 'admin', '0.0.0.0', '2019-12-18 12:23:23.313', '2019-12-18 12:23:23.313');
INSERT INTO `t_sys_perm` VALUES (32, '是否用户登陆后台', '系统用户权限管理', 'POST', 2, 5, '/sys/user/lockSysUserAdminLoginPerm', 10, 'com.jtw.sys.controller.user.SysUserController', 'modifySysUserAdminLoginPerm', 0, 1, 'admin', '0.0.0.0', '2019-12-18 12:23:23.313', '2019-12-18 12:23:23.313');
INSERT INTO `t_sys_perm` VALUES (33, '确认登陆密码重置申请', '系统用户基本信息管理', 'POST', 2, 5, '/sys/user/confirmPwdResetApply', 14, 'com.jtw.sys.controller.user.SysUserController', 'confirmPwdResetApply', 0, 1, 'admin', '0.0.0.0', '2019-12-18 12:23:23.313', '2019-12-18 12:23:23.313');
INSERT INTO `t_sys_perm` VALUES (34, '修改单个用户的密码', '系统用户基本信息管理', 'POST', 2, 5, '/sys/user/modifyAccountPwdByUserId', 9, 'com.jtw.sys.controller.user.SysUserController', 'modifyAccountPwdByUserId', 0, 1, 'admin', '0.0.0.0', '2019-12-18 12:23:23.313', '2019-12-18 12:23:23.313');
INSERT INTO `t_sys_perm` VALUES (35, '查询所有签到记录', '会员登陆可用组', 'GET', 2, 5, '/sys/user/findAllCheckInReward', 16, 'com.jtw.sys.controller.user.SysUserController', 'findAllCheckInReward', 0, 1, 'admin', '0.0.0.0', '2019-12-18 12:23:23.313', '2019-12-18 12:23:23.313');
INSERT INTO `t_sys_perm` VALUES (36, '拒绝登陆密码重置申请', '系统用户基本信息管理', 'POST', 2, 5, '/sys/user/refusedPwdResetApply', 14, 'com.jtw.sys.controller.user.SysUserController', 'refusedPwdResetApply', 0, 1, 'admin', '0.0.0.0', '2019-12-18 12:23:23.314', '2019-12-18 12:23:23.314');
INSERT INTO `t_sys_perm` VALUES (37, '获取单个用户【未拥有】的角色', '系统用户权限管理', 'GET', 2, 5, '/sys/user/getRolesByUserIdNotHave', 8, 'com.jtw.sys.controller.user.SysUserController', 'getRolesByUserIdNotHave', 0, 1, 'admin', '0.0.0.0', '2019-12-18 12:23:23.314', '2019-12-18 12:23:23.314');
INSERT INTO `t_sys_perm` VALUES (38, '查询所有登陆密码重置申请', '系统用户基本信息管理', 'GET', 2, 5, '/sys/user/findAllPwdResetApply', 12, 'com.jtw.sys.controller.user.SysUserController', 'findAllPwdResetApply', 0, 1, 'admin', '0.0.0.0', '2019-12-18 12:23:23.314', '2019-12-18 12:23:23.314');
INSERT INTO `t_sys_perm` VALUES (39, '查询所有计划任务', '定时任务管理', 'GET', 2, 6, '/sys/task/findAllTask', 4, 'com.jtw.sys.controller.task.taskController', 'findAllTask', 0, 1, 'admin', '0.0.0.0', '2019-12-18 12:23:23.314', '2019-12-18 12:23:23.314');
INSERT INTO `t_sys_perm` VALUES (40, '开启或者关闭一个定时任务', '定时任务管理', 'POST', 2, 6, '/sys/task/lockTask', 6, 'com.jtw.sys.controller.task.taskController', 'lockTask', 0, 1, 'admin', '0.0.0.0', '2019-12-18 12:23:23.315', '2019-12-18 12:23:23.315');
INSERT INTO `t_sys_perm` VALUES (41, '新增定时任务', '定时任务管理', 'POST', 2, 6, '/sys/task/addTask', 1, 'com.jtw.sys.controller.task.taskController', 'addTask', 0, 1, 'admin', '0.0.0.0', '2019-12-18 12:23:23.315', '2019-12-18 12:23:23.315');
INSERT INTO `t_sys_perm` VALUES (42, '修改定时任务信息', '定时任务管理', 'POST', 2, 6, '/sys/task/modifyTask', 5, 'com.jtw.sys.controller.task.taskController', 'modifyTask', 0, 1, 'admin', '0.0.0.0', '2019-12-18 12:23:23.315', '2019-12-18 12:23:23.315');
INSERT INTO `t_sys_perm` VALUES (43, '暂停一个定时任务', '定时任务管理', 'POST', 2, 6, '/sys/task/pauseTask', 2, 'com.jtw.sys.controller.task.taskController', 'pauseTask', 0, 1, 'admin', '0.0.0.0', '2019-12-18 12:23:23.315', '2019-12-18 12:23:23.315');
INSERT INTO `t_sys_perm` VALUES (44, '恢复一个定时任务', '定时任务管理', 'POST', 2, 6, '/sys/task/resumeTask', 3, 'com.jtw.sys.controller.task.taskController', 'resumeTask', 0, 1, 'admin', '0.0.0.0', '2019-12-18 12:23:23.315', '2019-12-18 12:23:23.315');
INSERT INTO `t_sys_perm` VALUES (45, '获取签到状态', '会员登陆可用组', 'GET', 2, 98, '/sys/member/getTodayCheckIn', 28, 'com.jtw.sys.controller.member.SysMemberController', 'getTodayCheckIn', 0, 1, 'admin', '0.0.0.0', '2019-12-18 12:23:23.315', '2019-12-18 12:23:23.315');
INSERT INTO `t_sys_perm` VALUES (46, '会员登陆入口', '公开权限组', 'POST', 2, 1, '/sys/member/login', 1, 'com.jtw.sys.controller.member.SysMemberController', 'login', 1, 1, 'admin', '0.0.0.0', '2019-12-18 12:23:23.315', '2019-12-18 12:23:23.315');
INSERT INTO `t_sys_perm` VALUES (47, '会员签到', '会员登陆可用组', 'POST', 2, 98, '/sys/member/memberCheckIn', 29, 'com.jtw.sys.controller.member.SysMemberController', 'memberCheckIn', 0, 1, 'admin', '0.0.0.0', '2019-12-18 12:23:23.315', '2019-12-18 12:23:23.315');
INSERT INTO `t_sys_perm` VALUES (48, '会员退出登录', '会员登陆可用组', 'POST', 2, 98, '/sys/member/logout', 1, 'com.jtw.sys.controller.member.SysMemberController', 'logout', 0, 1, 'admin', '0.0.0.0', '2019-12-18 12:23:23.316', '2019-12-18 12:23:23.316');
INSERT INTO `t_sys_perm` VALUES (49, '获取当前用户积分排名', '会员登陆可用组', 'GET', 2, 98, '/sys/member/getMyAssetRank', 5, 'com.jtw.sys.controller.member.SysMemberController', 'getMyAssetRank', 0, 1, 'admin', '0.0.0.0', '2019-12-18 12:23:23.316', '2019-12-18 12:23:23.316');
INSERT INTO `t_sys_perm` VALUES (50, '会员获取个人资产信息', '会员登陆可用组', 'GET', 2, 98, '/sys/member/getMyAsset', 5, 'com.jtw.sys.controller.member.SysMemberController', 'getMyAsset', 0, 1, 'admin', '0.0.0.0', '2019-12-18 12:23:23.316', '2019-12-18 12:23:23.316');
INSERT INTO `t_sys_perm` VALUES (51, '获取微信分享信息', '系统公开组', 'GET', 2, 1, '/sys/member/getWxShareConfig', 5, 'com.jtw.sys.controller.member.SysMemberController', 'getWxShareConfig', 1, 1, 'admin', '0.0.0.0', '2019-12-18 12:23:23.316', '2019-12-18 12:23:23.316');
INSERT INTO `t_sys_perm` VALUES (52, '公众号授权入口', '公开权限组', 'POST', 2, 1, '/sys/member/gzhlogin', 2, 'com.jtw.sys.controller.member.SysMemberController', 'gzhlogin', 1, 1, 'admin', '0.0.0.0', '2019-12-18 12:23:23.317', '2019-12-18 12:23:23.317');
INSERT INTO `t_sys_perm` VALUES (53, '查询积分排名', '会员登陆可用组', 'GET', 2, 98, '/sys/member/getAllAssetRank', 5, 'com.jtw.sys.controller.member.SysMemberController', 'getAllAssetRank', 0, 1, 'admin', '0.0.0.0', '2019-12-18 12:23:23.317', '2019-12-18 12:23:23.317');
INSERT INTO `t_sys_perm` VALUES (54, '会员注册入口', '公开权限组', 'POST', 2, 1, '/sys/member/sign', 1, 'com.jtw.sys.controller.member.SysMemberController', 'sign', 1, 1, 'admin', '0.0.0.0', '2019-12-18 12:23:23.317', '2019-12-18 12:23:23.317');
INSERT INTO `t_sys_perm` VALUES (55, '微信公众号分享回调', '系统公开组', 'POST', 2, 1, '/sys/member/wxgzhShareCallBack', 5, 'com.jtw.sys.controller.member.SysMemberController', 'wxgzhShareCallBack', 1, 1, 'admin', '0.0.0.0', '2019-12-18 12:23:23.317', '2019-12-18 12:23:23.317');
INSERT INTO `t_sys_perm` VALUES (56, '获取当前会员的基本信息', '会员登陆可用组', 'GET', 2, 98, '/sys/member/getMemberUserBaseInfoVo', 2, 'com.jtw.sys.controller.member.SysMemberController', 'getSysUserBaseInfoVo', 0, 1, 'admin', '0.0.0.0', '2019-12-18 12:23:23.317', '2019-12-18 12:23:23.317');
INSERT INTO `t_sys_perm` VALUES (57, '会员修改基本信息', '会员登陆可用组', 'POST', 2, 98, '/sys/member/modifyMemberUserBaseInfo', 19, 'com.jtw.sys.controller.member.SysMemberController', 'modifyMemberUserBaseInfo', 0, 1, 'admin', '0.0.0.0', '2019-12-18 12:23:23.317', '2019-12-18 12:23:23.317');
INSERT INTO `t_sys_perm` VALUES (58, '会员头像上传接口', '会员登陆可用组', 'POST', 2, 98, '/sys/member/uploadSingleHeadImage', 18, 'com.jtw.sys.controller.member.SysMemberController', 'uploadSingleHeadImage', 0, 1, 'admin', '0.0.0.0', '2019-12-18 12:23:23.318', '2019-12-18 12:23:23.318');
INSERT INTO `t_sys_perm` VALUES (59, '查询系统积分记录', '系统会员资金组', 'GET', 2, 11, '/sys/asset/findAllMemberAssetChangeLog', 7, 'com.jtw.sys.controller.asset.SysAssetController', 'findAllMemberAssetChangeLog', 0, 1, 'admin', '0.0.0.0', '2019-12-18 12:23:23.318', '2019-12-18 12:23:23.318');
INSERT INTO `t_sys_perm` VALUES (60, '会员积分调整', '系统会员资金组', 'POST', 2, 11, '/sys/asset/adjustMemberAsset', 9, 'com.jtw.sys.controller.asset.SysAssetController', 'adjustMemberAsset', 0, 1, 'admin', '0.0.0.0', '2019-12-18 12:23:23.318', '2019-12-18 12:23:23.318');
INSERT INTO `t_sys_perm` VALUES (61, '查询会员积分信息', '系统会员资金组', 'GET', 2, 11, '/sys/asset/findAllMemberAssetDetail', 8, 'com.jtw.sys.controller.asset.SysAssetController', 'findAllMemberAssetDetail', 0, 1, 'admin', '0.0.0.0', '2019-12-18 12:23:23.318', '2019-12-18 12:23:23.318');
INSERT INTO `t_sys_perm` VALUES (62, '查询所有资讯', '资讯管理组', 'GET', 2, 12, '/sys/information/findAllSysInformation', 3, 'com.jtw.sys.controller.information.SysInfoController', 'findAll', 0, 1, 'admin', '0.0.0.0', '2019-12-18 12:23:23.318', '2019-12-18 12:23:23.318');
INSERT INTO `t_sys_perm` VALUES (63, '获取单个资讯内容', '资讯管理组', 'GET', 2, 12, '/sys/information/getInformationById', 4, 'com.jtw.sys.controller.information.SysInfoController', 'getInformationById', 1, 1, 'admin', '0.0.0.0', '2019-12-18 12:23:23.319', '2019-12-18 12:23:23.319');
INSERT INTO `t_sys_perm` VALUES (64, '添加资讯', '资讯管理组', 'POST', 2, 12, '/sys/information/addSysInformation', 1, 'com.jtw.sys.controller.information.SysInfoController', 'addSysInformation', 0, 1, 'admin', '0.0.0.0', '2019-12-18 12:23:23.319', '2019-12-18 12:23:23.319');
INSERT INTO `t_sys_perm` VALUES (65, '修改资讯信息', '资讯管理组', 'POST', 2, 12, '/sys/information/modifySysInformation', 2, 'com.jtw.sys.controller.information.SysInfoController', 'modifySysInformation', 0, 1, 'admin', '0.0.0.0', '2019-12-18 12:23:23.319', '2019-12-18 12:23:23.319');
INSERT INTO `t_sys_perm` VALUES (66, '修改单条菜单信息', '系统菜单管理', 'POST', 2, 2, '/sys/menu/modifySysMenu', 4, 'com.jtw.sys.controller.menu.SysMenuController', 'modifySysMenu', 0, 1, 'admin', '0.0.0.0', '2019-12-18 12:23:23.319', '2019-12-18 12:23:23.319');
INSERT INTO `t_sys_perm` VALUES (67, '修改单个角色菜单', '系统菜单管理', 'POST', 2, 2, '/sys/menu/modifyRoleMenus', 6, 'com.jtw.sys.controller.menu.SysMenuController', 'modifyRoleMenus', 0, 1, 'admin', '0.0.0.0', '2019-12-18 12:23:23.319', '2019-12-18 12:23:23.319');
INSERT INTO `t_sys_perm` VALUES (68, '删除单条系统菜单', '系统菜单管理', 'POST', 2, 2, '/sys/menu/delSysMenu', 5, 'com.jtw.sys.controller.menu.SysMenuController', 'delSysMenu', 0, 1, 'admin', '0.0.0.0', '2019-12-18 12:23:23.319', '2019-12-18 12:23:23.319');
INSERT INTO `t_sys_perm` VALUES (69, '添加系统菜单', '系统菜单管理', 'POST', 2, 2, '/sys/menu/addSysMenu', 3, 'com.jtw.sys.controller.menu.SysMenuController', 'addSysMenu', 0, 1, 'admin', '0.0.0.0', '2019-12-18 12:23:23.320', '2019-12-18 12:23:23.320');
INSERT INTO `t_sys_perm` VALUES (70, '查询系统菜单表中的记录', '系统菜单管理', 'GET', 2, 2, '/sys/menu/findAllSysMenus', 2, 'com.jtw.sys.controller.menu.SysMenuController', 'findAllSysMenus', 0, 1, 'admin', '0.0.0.0', '2019-12-18 12:23:23.320', '2019-12-18 12:23:23.320');
INSERT INTO `t_sys_perm` VALUES (71, '获取所有系统菜单', '系统菜单管理', 'GET', 2, 2, '/sys/menu/getAllSysMenus', 1, 'com.jtw.sys.controller.menu.SysMenuController', 'getAllSysMenus', 0, 1, 'admin', '0.0.0.0', '2019-12-18 12:23:23.320', '2019-12-18 12:23:23.320');
INSERT INTO `t_sys_perm` VALUES (72, '获取角色所有菜单并标记', '系统菜单管理', 'GET', 2, 2, '/sys/menu/getAllSysMenuTreeMarkByRoleId', 6, 'com.jtw.sys.controller.menu.SysMenuController', 'getAllSysMenuTreeMarkByRoleId', 0, 1, 'admin', '0.0.0.0', '2019-12-18 12:23:23.320', '2019-12-18 12:23:23.320');
INSERT INTO `t_sys_perm` VALUES (73, '获取用户的菜单树', '系统登陆可用组', 'GET', 2, 99, '/sys/menu/getAllMenusTreeByAccount', 2, 'com.jtw.sys.controller.menu.SysMenuController', 'getAllMenusTreeByAccount', 0, 1, 'admin', '0.0.0.0', '2019-12-18 12:23:23.320', '2019-12-18 12:23:23.320');
INSERT INTO `t_sys_perm` VALUES (74, '系统配置添加操作', '系统配置项管理', 'POST', 2, 10, '/sys/config/addConfig', 1, 'com.jtw.sys.controller.system.ConfigController', 'addConfig', 0, 1, 'admin', '0.0.0.0', '2019-12-18 12:23:23.321', '2019-12-18 12:23:23.321');
INSERT INTO `t_sys_perm` VALUES (75, '系统配置修改操作', '系统配置项管理', 'POST', 2, 10, '/sys/config/modifyConfigById', 2, 'com.jtw.sys.controller.system.ConfigController', 'modifyConfigById', 0, 1, 'admin', '0.0.0.0', '2019-12-18 12:23:23.321', '2019-12-18 12:23:23.321');
INSERT INTO `t_sys_perm` VALUES (76, '查询所有系统配置', '系统配置项管理', 'GET', 2, 10, '/sys/config/findAllSysConfig', 3, 'com.jtw.sys.controller.system.ConfigController', 'findAllSysConfig', 0, 1, 'admin', '0.0.0.0', '2019-12-18 12:23:23.321', '2019-12-18 12:23:23.321');
INSERT INTO `t_sys_perm` VALUES (77, '获取系统配置信息', '系统公开组', 'GET', 2, 1, '/sys/config/getAllSysConfig', 9, 'com.jtw.sys.controller.system.ConfigController', 'getAllSysConfig', 1, 1, 'admin', '0.0.0.0', '2019-12-18 12:23:23.321', '2019-12-18 12:23:23.321');
INSERT INTO `t_sys_perm` VALUES (78, '保存网站LOGO', '客户端配置管理', 'POST', 2, 13, '/sys/config/saveWebLogo', 4, 'com.jtw.sys.controller.system.ConfigController', 'saveWebLogo', 0, 1, 'admin', '0.0.0.0', '2019-12-18 12:23:23.321', '2019-12-18 12:23:23.321');
INSERT INTO `t_sys_perm` VALUES (79, '获取手机端轮播图列表', '系统公开组', 'GET', 2, 1, '/sys/config/getAllMobileClientBanner', 10, 'com.jtw.sys.controller.system.ConfigController', 'getAllMobileClientBanner', 1, 1, 'admin', '0.0.0.0', '2019-12-18 12:23:23.321', '2019-12-18 12:23:23.321');
INSERT INTO `t_sys_perm` VALUES (80, '保存移动客户端背景图', '客户端配置管理', 'POST', 2, 13, '/sys/config/saveClientBgImageInfo', 4, 'com.jtw.sys.controller.system.ConfigController', 'saveClientBgImageInfo', 0, 1, 'admin', '0.0.0.0', '2019-12-18 12:23:23.322', '2019-12-18 12:23:23.322');
INSERT INTO `t_sys_perm` VALUES (81, '保存PC客户端背景图', '客户端配置管理', 'POST', 2, 13, '/sys/config/saveClientBgImageInfoPC', 5, 'com.jtw.sys.controller.system.ConfigController', 'saveClientBgImageInfoPC', 0, 1, 'admin', '0.0.0.0', '2019-12-18 12:23:23.322', '2019-12-18 12:23:23.322');
INSERT INTO `t_sys_perm` VALUES (82, '封面上传接口', '单文件上传', 'POST', 2, 7, '/sys/file/uploadGameImage', 1, 'com.jtw.sys.controller.file.fileController', 'uploadGameImage', 0, 1, 'admin', '0.0.0.0', '2019-12-18 12:23:23.322', '2019-12-18 12:23:23.322');
INSERT INTO `t_sys_perm` VALUES (83, '删除日志文件', '日志文件管理', 'POST', 2, 7, '/sys/file/delAllSysLogFile', 7, 'com.jtw.sys.controller.file.fileController', 'delAllSysLogFile', 0, 1, 'admin', '0.0.0.0', '2019-12-18 12:23:23.322', '2019-12-18 12:23:23.322');
INSERT INTO `t_sys_perm` VALUES (84, '获取所有日志文件', '日志文件管理', 'GET', 2, 7, '/sys/file/getAllSysLogFile', 4, 'com.jtw.sys.controller.file.fileController', 'getAllSysLogFile', 0, 1, 'admin', '0.0.0.0', '2019-12-18 12:23:23.322', '2019-12-18 12:23:23.322');
INSERT INTO `t_sys_perm` VALUES (85, '头像上传接口', '系统登陆可用组', 'POST', 2, 99, '/sys/file/uploadSingleHeadImage', 4, 'com.jtw.sys.controller.file.fileController', 'uploadSingleHeadImage', 0, 1, 'admin', '0.0.0.0', '2019-12-18 12:23:23.323', '2019-12-18 12:23:23.323');
INSERT INTO `t_sys_perm` VALUES (86, '银行卡图片上传接口', '单文件上传', 'POST', 2, 7, '/sys/file/uploadSysBankImage', 1, 'com.jtw.sys.controller.file.fileController', 'uploadSysBankImage', 0, 1, 'admin', '0.0.0.0', '2019-12-18 12:23:23.323', '2019-12-18 12:23:23.323');
INSERT INTO `t_sys_perm` VALUES (87, '客户端背景图管理', '单文件上传', 'POST', 2, 7, '/sys/file/uploadClientBgImage', 4, 'com.jtw.sys.controller.file.fileController', 'uploadClientBgImage', 0, 1, 'admin', '0.0.0.0', '2019-12-18 12:23:23.323', '2019-12-18 12:23:23.323');
INSERT INTO `t_sys_perm` VALUES (88, '网站logo修改', '单文件上传', 'POST', 2, 7, '/sys/file/uploadWebLogoImage', 4, 'com.jtw.sys.controller.file.fileController', 'uploadWebLogoImage', 0, 1, 'admin', '0.0.0.0', '2019-12-18 12:23:23.323', '2019-12-18 12:23:23.323');
INSERT INTO `t_sys_perm` VALUES (89, '登录验证码', '获取系统登录验证码', 'GET', 2, 1, '/sys/captcha/getLoginCaptcha', 4, 'com.jtw.sys.controller.CaptchaController', 'getLoginCaptcha', 1, 1, 'admin', '0.0.0.0', '2019-12-18 12:23:23.324', '2019-12-18 12:23:23.324');

-- ----------------------------
-- Table structure for t_sys_perm_group
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_perm_group`;
CREATE TABLE `t_sys_perm_group`  (
  `ID` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '权限组ID主键',
  `GROUP` tinyint(2) UNSIGNED NOT NULL COMMENT '权限分组ID',
  `DESC` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '默认:1、公开组2、菜单组3、权限组4、角色组5、系统用户组99、管理用户登陆可用98、会员用户登陆可用',
  PRIMARY KEY (`ID`) USING BTREE,
  UNIQUE INDEX `GROUP`(`GROUP`, `DESC`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 15 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '系统权限分组表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of t_sys_perm_group
-- ----------------------------
INSERT INTO `t_sys_perm_group` VALUES (3, 1, '公开组');
INSERT INTO `t_sys_perm_group` VALUES (4, 2, '系统菜单组');
INSERT INTO `t_sys_perm_group` VALUES (5, 3, '系统权限组');
INSERT INTO `t_sys_perm_group` VALUES (6, 4, '系统角色组');
INSERT INTO `t_sys_perm_group` VALUES (7, 5, '系统用户信息组');
INSERT INTO `t_sys_perm_group` VALUES (8, 6, '系统定时任务组');
INSERT INTO `t_sys_perm_group` VALUES (9, 7, '系统文件操作组');
INSERT INTO `t_sys_perm_group` VALUES (10, 10, '系统配置项组');
INSERT INTO `t_sys_perm_group` VALUES (11, 11, '会员资金组');
INSERT INTO `t_sys_perm_group` VALUES (12, 12, '系统资讯组');
INSERT INTO `t_sys_perm_group` VALUES (13, 13, '客户端设置组');
INSERT INTO `t_sys_perm_group` VALUES (14, 14, '客服管理组');
INSERT INTO `t_sys_perm_group` VALUES (1, 98, '会员登陆可用组');
INSERT INTO `t_sys_perm_group` VALUES (2, 99, '系统登陆可用组');

-- ----------------------------
-- Table structure for t_sys_role
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_role`;
CREATE TABLE `t_sys_role`  (
  `ID` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '角色ID主键',
  `NAME` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '系统角色名称',
  `DESC` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '' COMMENT '角色描述',
  `OPERATOR` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '操作者',
  `OPERATOR_IP` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '最后一次更新者的ip地址',
  `CREATE_TIME` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) COMMENT '添加时间',
  `UPDATE_TIME` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3) COMMENT '最后更新时间',
  PRIMARY KEY (`ID`) USING BTREE,
  UNIQUE INDEX `NAME`(`NAME`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '系统角色表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of t_sys_role
-- ----------------------------
INSERT INTO `t_sys_role` VALUES (1, '超级管理员', '系统最高级别角色(请勿删除)', 'admin', '0.0.0.0', '2019-12-18 12:23:23.646', '2019-12-18 12:23:23.646');
INSERT INTO `t_sys_role` VALUES (2, '会员', '系统会员角色(请勿删除)', 'admin', '127.0.0.1', '2019-12-18 12:23:23.745', '2019-12-18 12:23:23.745');

-- ----------------------------
-- Table structure for t_sys_task
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_task`;
CREATE TABLE `t_sys_task`  (
  `ID` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `NAME` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '任务名称',
  `GROUP_NAME` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '任务组',
  `CRON` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '任务的执行周期，例如:\"0/120 * * * * ?\"',
  `GAME_ID` int(11) UNSIGNED NOT NULL COMMENT '游戏的主键id',
  `TASK_STATE` tinyint(4) UNSIGNED NOT NULL DEFAULT 0 COMMENT '任务发布状态',
  `RUN_STATE` tinyint(4) UNSIGNED NOT NULL DEFAULT 0 COMMENT '任务运行状态',
  `PARAM` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '任务的参数',
  `METHOD_NAME` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '任务调用的方法',
  `DESCRIPT` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '任务描述',
  `BEAN_CLASS` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '任务实例bean的类',
  `CREATE_ID` int(11) UNSIGNED NOT NULL COMMENT '创建着用户id',
  `UPDATE_ID` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '更新用户id',
  `CREATE_TIME` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) COMMENT '添加时间',
  `UPDATE_TIME` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3) COMMENT '最后更新时间',
  PRIMARY KEY (`ID`) USING BTREE,
  UNIQUE INDEX `NAME`(`NAME`, `GAME_ID`) USING BTREE,
  INDEX `CREATE_ID`(`CREATE_ID`, `UPDATE_ID`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '系统定时任务表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for t_sys_task_log
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_task_log`;
CREATE TABLE `t_sys_task_log`  (
  `ID` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `TASK_ID` int(11) UNSIGNED NOT NULL COMMENT '主键',
  `GAME_ID` int(11) UNSIGNED NOT NULL COMMENT '主键',
  `NAME` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '任务名',
  `DESCRIPT` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '任务描述',
  `STATE` tinyint(4) UNSIGNED NOT NULL COMMENT '任务发布状态',
  `CREATE_TIME` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) COMMENT '添加时间',
  PRIMARY KEY (`ID`) USING BTREE,
  INDEX `GAME_ID`(`GAME_ID`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '系统定时任务日志表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for t_sys_user
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_user`;
CREATE TABLE `t_sys_user`  (
  `ID` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '用户ID主键',
  `USER_NAME` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '用户名',
  `PASSWORD` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '密码',
  `SALT` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '密码盐',
  `STATE` tinyint(4) UNSIGNED NOT NULL DEFAULT 0 COMMENT '账号状态：0为正常,1为锁定,2为删除',
  `OPERATOR` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '操作者',
  `OPERATOR_IP` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '最后一次更新者的ip地址',
  `CREATE_TIME` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) COMMENT '第一次添加时间',
  `UPDATE_TIME` datetime(3) NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3) COMMENT '最后更新时间',
  PRIMARY KEY (`ID`) USING BTREE,
  UNIQUE INDEX `USER_NAME`(`USER_NAME`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '系统角色表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of t_sys_user
-- ----------------------------
INSERT INTO `t_sys_user` VALUES (1, 'admin', 'f2a0f081b46af688e7c8c795cc007450', '83c9a13a35e8b7024f4e7277c4f61e30', 0, 'admin', '0.0.0.0', '2019-12-18 12:23:23.436', '2019-12-18 12:23:23.436');

-- ----------------------------
-- Table structure for t_sys_user_info
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_user_info`;
CREATE TABLE `t_sys_user_info`  (
  `ID` int(11) UNSIGNED NOT NULL COMMENT '用户信息id与用户表相同',
  `NICK_NAME` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '' COMMENT '用户昵称',
  `HEAD_IMAGE` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '' COMMENT '用户头像地址',
  `THUMB_NAIL_IMAGE` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '' COMMENT '头像缩略图地址',
  `SEX` tinyint(4) UNSIGNED NOT NULL DEFAULT 0 COMMENT '性别:1：男，2：女,0：保密',
  `AGE` int(3) UNSIGNED NULL DEFAULT NULL COMMENT '年龄',
  `MOBILE` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '' COMMENT '手机号码',
  `EMAIL` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '' COMMENT '邮箱',
  `QQ` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '' COMMENT 'QQ号码',
  `DESC` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '' COMMENT '用户描述信息',
  `LAST_LOGIN_TIME` datetime(3) NULL DEFAULT NULL COMMENT '上次登录时间',
  `CURRENT_LOGIN_TIME` datetime(3) NULL DEFAULT NULL COMMENT '当前登录时间',
  `ADDRESS` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '' COMMENT '用户地址',
  `OPERATOR` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '操作者',
  `OPERATOR_IP` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '最后一次更新者的ip地址',
  `CREATE_TIME` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) COMMENT '添加时间',
  `UPDATE_TIME` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3) COMMENT '最后更新时间',
  PRIMARY KEY (`ID`) USING BTREE,
  INDEX `MOBILE`(`MOBILE`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '用户信息表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of t_sys_user_info
-- ----------------------------
INSERT INTO `t_sys_user_info` VALUES (1, 'admin', NULL, NULL, 3, NULL, NULL, NULL, NULL, NULL, NULL, '2019-12-18 12:23:51.974', NULL, 'admin', '0.0.0.0', '2019-12-18 12:23:23.436', '2019-12-18 12:23:51.976');

-- ----------------------------
-- Table structure for t_sys_user_login_log
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_user_login_log`;
CREATE TABLE `t_sys_user_login_log`  (
  `ID` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '登录日志主键id',
  `USER_ID` int(11) UNSIGNED NOT NULL COMMENT '用户id',
  `IP` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '登录ip地址',
  `ADDRESS` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '登陆地址',
  `DEVICE` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '登陆设备名称',
  `LOGIN_MODE` int(2) NOT NULL DEFAULT 1 COMMENT '登陆方式，1为浏览器，2为App登陆',
  `LOGIN_TIME` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) COMMENT '登录时间',
  PRIMARY KEY (`ID`) USING BTREE,
  INDEX `USER_ID`(`USER_ID`) USING BTREE,
  INDEX `DEVICE`(`DEVICE`) USING BTREE,
  INDEX `ADDRESS`(`ADDRESS`) USING BTREE,
  INDEX `LOGIN_TIME`(`LOGIN_TIME`) USING BTREE,
  INDEX `IP`(`IP`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '用户登录信息记录表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of t_sys_user_login_log
-- ----------------------------
INSERT INTO `t_sys_user_login_log` VALUES (1, 1, '0:0:0:0:0:0:0:1', '0|0|0|内网IP|内网IP', 'Windows 10', 1, '2019-12-18 12:23:52.045');

-- ----------------------------
-- Table structure for t_sys_user_open
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_user_open`;
CREATE TABLE `t_sys_user_open`  (
  `ID` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `USER_ID` int(11) UNSIGNED NOT NULL COMMENT '用户id',
  `WX_GZH_OPEN_ID` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '' COMMENT '微信公众平台(web)openid',
  `WX_APP_OPEN_ID` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '' COMMENT '微信开放平台(APP)openid',
  `QQ_OPEN_ID` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '' COMMENT 'QQ开放平台openid',
  `ZFB_OPEN_ID` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '' COMMENT '支付宝开放平台openid',
  `TX_UNION_ID` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '' COMMENT '腾讯互联平台unionid',
  `SINA_OPEN_ID` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '' COMMENT '新浪微博unionid',
  `CREATE_TIME` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) COMMENT '第一次添加时间',
  `UPDATE_TIME` datetime(3) NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3) COMMENT '最后更新时间',
  PRIMARY KEY (`ID`) USING BTREE,
  INDEX `USER_ID`(`USER_ID`) USING BTREE,
  INDEX `WX_GZH_OPEN_ID`(`WX_GZH_OPEN_ID`) USING BTREE,
  INDEX `TX_UNION_ID`(`TX_UNION_ID`) USING BTREE,
  INDEX `WX_APP_OPEN_ID`(`WX_APP_OPEN_ID`) USING BTREE,
  INDEX `QQ_OPEN_ID`(`QQ_OPEN_ID`) USING BTREE,
  INDEX `ZFB_OPEN_ID`(`ZFB_OPEN_ID`) USING BTREE,
  INDEX `SINA_OPEN_ID`(`SINA_OPEN_ID`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '第三方平台授权扩展表' ROW_FORMAT = Compact;

SET FOREIGN_KEY_CHECKS = 1;

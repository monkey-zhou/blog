package com.jtw.common.jwt;

import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;

/**
 * DESCRIPT :
 *
 * @author : cjsky666
 * @version :1.0.1
 * @date : Created at 2019/12/17 17:52
 * modify By:
 */
public class MouseCtrl {

    public static final Dimension dim; //存储屏幕尺寸
    public static Robot robot = null;//自动化对象
    static {
        dim = Toolkit.getDefaultToolkit().getScreenSize();
        System.out.println("当前屏幕大小为：" + dim.getWidth() + " " + dim.getHeight());
        try {
            robot = new Robot();
        } catch (AWTException e) {
            e.printStackTrace();
        }
    }
    public static void printMouseLocation(){
        Point point = MouseInfo.getPointerInfo().getLocation();
        System.out.println("当前鼠标位置 "+point.x+","+point.y);
    }
    //鼠标移动函数
    public static void Move(int x,int y){
        try{
            robot.mouseMove(0,0);
            robot.mouseMove((int) (x/1.25),(int) (y/1.25));
            robot.delay(2500);
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    /**
     * Shift组合键
     */
    public static void keyPressWithShift(int key){
        //按下Shift
        robot.keyPress(KeyEvent.VK_SHIFT);
        //按下某个键
        robot.keyPress(key);

        //释放某个键
        robot.keyRelease(key);
        //释放Shift
        robot.keyRelease(KeyEvent.VK_SHIFT);
        //等待100ms
        robot.delay(100);
    }

    /**
     * Ctrl组合键
     * @param key
     */
    public static void keyPressWithCtrl(int key){
        robot.keyPress(KeyEvent.VK_CONTROL);
        robot.keyPress(key);

        robot.keyRelease(key);
        robot.keyRelease(KeyEvent.VK_CONTROL);

        robot.delay(100);
    }

    /**
     * Alt组合键
     * @param key
     */
    public static void keyPressWithAlt(int key) {
        robot.keyPress(KeyEvent.VK_ALT);
        robot.keyPress(key);

        robot.keyRelease(key);
        robot.keyRelease(KeyEvent.VK_ALT);
        robot.delay(100);
    }
    /**
     * 把文本设置到剪贴板（复制）
     */
    public static void setClipboardString(String text) {
        // 获取系统剪贴板
        Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
        // 封装文本内容
        Transferable trans = new StringSelection(text);
        // 把文本内容设置到系统剪贴板
        clipboard.setContents(trans, null);
    }
    /**
     * 从剪贴板中获取文本（粘贴）
     */
    public static String getClipboardString() {
        // 获取系统剪贴板
        Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
        // 获取剪贴板中的内容
        Transferable trans = clipboard.getContents(null);
        if (trans != null) {
            // 判断剪贴板中的内容是否支持文本
            if (trans.isDataFlavorSupported(DataFlavor.stringFlavor)) {
                try {
                    // 获取剪贴板中的文本内容
                    String text = (String) trans.getTransferData(DataFlavor.stringFlavor);
                    return text;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }
}

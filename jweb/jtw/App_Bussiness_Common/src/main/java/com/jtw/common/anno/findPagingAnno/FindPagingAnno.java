package com.jtw.common.anno.findPagingAnno;


import java.lang.annotation.*;

/**
 *
 * @author cjsky666
 * @date 2018/11/3 22:43
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface FindPagingAnno {
        FindPagingItemAnno [] items();
}

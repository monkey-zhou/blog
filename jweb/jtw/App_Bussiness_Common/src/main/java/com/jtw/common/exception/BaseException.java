package com.jtw.common.exception;

import java.util.Calendar;

/**
 * Created by keliii on 2018/7/17.
 */
public class BaseException extends RuntimeException {

    private Integer code;
    private String message;
    private Object data;
    private long serviceTime;

    public BaseException() {
        this.serviceTime = Calendar.getInstance().getTimeInMillis();
    }

    public BaseException(Integer code, String message, Object data) {
        this.code = code;
        this.message = message;
        this.data = data;
        this.serviceTime = Calendar.getInstance().getTimeInMillis();
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}

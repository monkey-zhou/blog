package com.jtw.common.anno.RequestAnno;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * DESCRIPT:接口权限初始化注解配置
 *  open 接口是否公开
 *  url 接口的路径
 *  state 接口的状态 1为需要配置角色验证 ，2为登陆可用，0为禁用
 *  type' 权限类型 1、菜单2、按钮、3其他',
 *  group '权限组1、公开组2、菜单组3、权限组4、角色组5、用户组99、登陆可用',
 * @author cjsky666
 * @date 2018/11/15 8:38
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface PermAnno {
    /**
     * 是否为公开权限
     * @return
     */
    boolean open() default false;

    /**
     * 权限接口的状态 1为需要配置角色验证 ，2为登陆可用，0为禁用
     * @return
     */
    int state() default 1;

    /**
     * 权限组内的排序
     * @return
     */
    int sort() default 0;

    /**
     * 权限类型2为按钮
     * @return
     */
    int type() default 2;

    /**
     * 权限分组标志
     * @return
     */
    int group();
}

package com.jtw.common.bean.enums;


import lombok.Data;

/**
 * DESCRIPT:平台权限组设定
 *
 * @author cjsky666
 * @date 2018/11/15 14:06
 */

public enum PermGroup {
    MemberAuthc(98, "会员登陆可用组"),
    SysAuthc(99, "系统登陆可用组"),
    Open(1, "公开组"),
    Menu(2, "系统菜单组"),
    Perm(3, "系统权限组"),
    Role(4, "系统角色组"),
    SysUser(5, "系统用户信息组"),
    SysTask(6, "系统定时任务组"),
    SysFile(7, "系统文件操作组"),
    SysConfig(10, "系统配置项组"),
    SysMemberAsset(11, "会员资金组"),
    SysInformation(12, "系统资讯组"),
    SysClient(13, "客户端设置组"),
    IMService(14, "客服管理组"),
    ;
    private Integer group;
    private String desc;
    PermGroup(Integer group, String desc) {
        this.group = group;
        this.desc = desc;
    }

    public Integer getGroup() {
        return group;
    }

    public void setGroup(Integer group) {
        this.group = group;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public static void main(String[] args) {
        for(PermGroup permGroup:PermGroup.values()){
            System.out.println(permGroup);
        }
    }
}

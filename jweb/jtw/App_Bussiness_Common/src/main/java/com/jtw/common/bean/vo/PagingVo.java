package com.jtw.common.bean.vo;

import lombok.Data;

/**
 * @author cjsky666
 * @date
 */
@Data
public class PagingVo {
	private Integer page = 1;
    private Integer rows = 10;
    private String orderBy;
}

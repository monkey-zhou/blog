package com.jtw.conf.datasource;

import com.alibaba.druid.pool.DruidDataSource;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;


/**
 * DESCRIPT:druid数据库数据源
 *
 * @author cjsky666
 * @date 2018-8-27 16:52
 */
@Configuration
public class datasourceConf {
    @Bean
    @ConfigurationProperties(prefix = "druid.datasource")
    public DataSource dataSource() {
        return DataSourceBuilder.create().type(DruidDataSource.class).build();
    }
}

package com.jtw.conf.shiro.enums;

/**
 * DESCRIPT:
 *
 * @author cjsky666
 * @date 2019/7/24 22:01
 */
public enum LoginType {
    PASSWORD("password"), // 密码登录
    NOPASSWD("nopassword"), // 账号免密登录
    WXAPP("wxapp"), // 微信app登录
    WXGZH("wxgzh"), // 微信公众登录
    QQ("qq"), // 微信公众登录
    ZFB("zfb"), // 微信公众登录
    MOBILE("mobile"), // 手机号免密登录
    TX_UNION("txunino"), // 腾讯互联账号id
    SINA("sina"); // 新浪微博

    private String code;// 状态值

    private LoginType(String code) {
        this.code = code;
    }
    public String getCode () {
        return code;
    }
}

package com.jtw.conf.shiro.service;

import com.jtw.conf.shiro.model.BaseRole;
import com.jtw.conf.shiro.model.BaseUser;
import com.jtw.conf.shiro.model.FilterPerm;

import java.util.List;
import java.util.Set;

/**
 * DESCRIPT:账户基本信息查询
 *
 * @author cjsky666
 * @date 2018/9/3 11:12
 */
public interface BasePermService {
    /**
     * 根据用户名获取当前用户所有角色
     * @param userName
     * @return
     */
    Set<BaseRole> getRolesByUserName(String userName);

    /**
     * 根据用户名查找Auth用户
     * @param userName
     * @return
     */
    BaseUser getUserByUserName(String userName);
    /**
     * 获取当前系统内的权限信息
     */
    List<FilterPerm> getAllPerm();
    /**
     * 根据手机号查询基本用户信息
     * @param mobile
     * @return
     */
    BaseUser getUserByMobile(String mobile);

    /**
     * 根据手机号查询基本用户信息
     * @param wxAppOpenId
     * @return
     */
    BaseUser getUserByWXAPP(String wxAppOpenId);

    /**
     * 根据微信公众号openid查询用户信息
     * @param wxGzhOpenId
     * @return
     */
    BaseUser getUserByWXGZH(String wxGzhOpenId);
    /**
     * 根据腾讯互联系统账号唯一标识id，包含（
     *  微信开放平台openId、
     *  QQ授权id、
     *  微信公众平openId获取用户信息
     * ）
     * @param txUnionId
     * @return
     */
    BaseUser getUserByTXUNION(String txUnionId);
    /**
     * 根据支付宝平台授权id获取用户信息
     * @param zfbOpenId
     * @return
     */
    BaseUser getUserByZFB(String zfbOpenId);
    /**
     * 根据QQ开放平台授权openid获取用户信息
     * @param qqOpenId
     * @return
     */
    BaseUser getUserByQQ(String qqOpenId);
    /**
     * 根据新浪微博开放平台授权openid获取用户信息
     * @param sinaOpenId
     * @return
     */
    BaseUser getUserBySina(String sinaOpenId);
}

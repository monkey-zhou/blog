package com.jtw.conf.shiro.model;

import lombok.Data;
/**
 * DESCRIPT:基础角色
 *
 * @author cjsky666
 * @date 2018/9/3 10:34
 */
@Data
public class BaseRole {
    private Integer id;
    private String name;
    private String desc;


}

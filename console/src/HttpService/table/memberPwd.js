export default {
  userName: {
    params:{
      value:"",
      column:"userName",
      condition:"LIKE",
      prefix:""
    },
    orderBy:{
      state:0,
      index:0,
      value:'',
      options:[]
    },
    alias:'包含',
    state:0,
    label:"用户名",
    type:1,
    conditions:{
      options:[
        {opt:"LIKE",alias:"包含"},
        {opt:"=",alias:"等于"}
      ]
    }
  },
  state: {
    params:{
      value:"",
      column:"state",
      condition:"=",
      prefix:""
    },
    orderBy:{
      state:0,
      index:0,
      value:'',
      options:[]
    },
    alias:'',
    state:0,
    label:"申请状态",
    type:3,
    conditions:{
      options:[
        {opt:"0",alias:"待处理"},
        {opt:"1",alias:"已确认"},
        {opt:"2",alias:"已拒绝"}
      ]
    }
  },
  reason: {
    params:{
      value:"",
      column:"reason",
      condition:"LIKE",
      prefix:""
    },
    orderBy:{
      state:0,
      index:0,
      value:'',
      options:[]
    },
    alias:'包含',
    state:0,
    label:"申请理由",
    type:1,
    conditions:{
      options:[
        {opt:"LIKE",alias:"包含"},
        {opt:"=",alias:"等于"}
      ]
    }
  },
  createTime: {
    params: {
      column: "createTime"
    },
    orderBy: {
      state: 1,
      value: 'desc',
      options: [
        {opt: 'desc', alias: '降序'},
        {opt: 'asc', alias: '升序'}
      ]
    },
    label: "申请时间",
    type: 2,//时间筛选类型
    filter: [
      {
        params: {
          value: "",
          column: "createTime",
          condition: ">=",
          prefix: "`t_sys_member_pwd_reset`.",
        },
        alias: '大于等于',
        state: 0,
        value: "00:00:00",
        label: "开始时间",
        conditions: {
          options: [
            {opt: ">=", alias: "大于等于"},
            {opt: "=", alias: "等于"},
            {opt: ">", alias: "大于"}
          ]
        }
      },
      {
        params: {
          value: "",
          column: "createTime",
          condition: "<=",
          prefix: "`t_sys_member_pwd_reset`.",
        },
        alias: '小于等于',
        state: 0,
        label: "结束时间",
        value: "23:59:59",
        conditions: {
          options: [
            {opt: "<=", alias: "小于等于"},
            {opt: "=", alias: "等于"},
            {opt: "<", alias: "小于"}
          ]
        }
      }
    ]
  }
};

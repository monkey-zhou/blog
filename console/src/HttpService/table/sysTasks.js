// view_list_sysTask.vue
export default {
  name: {
    params:{
      value:"",
      column:"name",
      condition:"LIKE",
      prefix:""
    },
    orderBy:{
      state:0,
      index:0,
      value:'',
      options:[]
    },
    alias:'包含',
    state:0,
    label:"任务名",
    type:1,//输入框类型
    conditions:{
      options:[
        {opt:"LIKE",alias:"包含"},
        {opt:"=",alias:"等于"},
      ]
    }
  },
  groupName: {
    params:{
      value:"",
      column:"groupName",
      condition:"LIKE",
      prefix:""
    },
    orderBy:{
      state:0,
      index:0,
      value:'',
      options:[]
    },
    alias:'包含',
    state:0,
    label:"任务组",
    type:1,//输入框类型
    conditions:{
      options:[
        {opt:"LIKE",alias:"包含"},
        {opt:"=",alias:"等于"},
      ]
    }
  },
  methodName: {
    params:{
      value:"",
      column:"methodName",
      condition:"LIKE",
      prefix:""
    },
    orderBy:{
      state:0,
      index:0,
      value:'',
      options:[]
    },
    alias:'包含',
    state:0,
    label:"方法名",
    type:1,//输入框类型
    conditions:{
      options:[
        {opt:"LIKE",alias:"包含"},
        {opt:"=",alias:"等于"},
      ]
    }
  },
  cron: {
    params:{
      value:"",
      column:"cron",
      condition:"LIKE",
      prefix:""
    },
    orderBy:{
      state:0,
      index:0,
      value:'',
      options:[]
    },
    alias:'包含',
    state:0,
    label:"定时周期",
    type:1,//输入框类型
    conditions:{
      options:[
        {opt:"LIKE",alias:"包含"},
        {opt:"=",alias:"等于"},
      ]
    }
  },
  param: {
    params:{
      value:"",
      column:"param",
      condition:"LIKE",
      prefix:""
    },
    orderBy:{
      state:0,
      index:0,
      value:'',
      options:[]
    },
    alias:'包含',
    state:0,
    label:"任务参数",
    type:1,//输入框类型
    conditions:{
      options:[
        {opt:"LIKE",alias:"包含"},
        {opt:"=",alias:"等于"},
      ]
    }
  },
  taskState: {
    params:{
      value:"",
      column:"taskState",
      condition:"=",
      prefix:""
    },
    orderBy:{
      state:0,
      index:0,
      value:'',
      options:[]
    },
    alias:'等于',
    state:0,
    label:"任务状态",
    type:3,
    conditions:{
      options:[
        {opt:"1",alias:"已开启"},
        {opt:"0",alias:"未开启"}
      ]
    }
  },
  planState: {
    params:{
      value:"",
      column:"planState",
      condition:"=",
      prefix:""
    },
    orderBy:{
      state:0,
      index:0,
      value:'',
      options:[]
    },
    alias:'等于',
    state:0,
    label:"计划状态",
    type:3,
    conditions:{
      options:[
        {opt:"1",alias:"已开启"},
        {opt:"0",alias:"未开启"}
      ]
    }
  },
  runState: {
    params:{
      value:"",
      column:"runState",
      condition:"=",
      prefix:""
    },
    orderBy:{
      state:0,
      index:0,
      value:'',
      options:[]
    },
    alias:'等于',
    state:0,
    label:"运行状态",
    type:3,
    conditions:{
      options:[
        {opt:"1",alias:"已开启"},
        {opt:"0",alias:"未开启"}
      ]
    }
  }
};

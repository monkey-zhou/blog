//js倒计时多线程程序插件。规避单线程倒计时页面卡顿和延迟的问题
var addZero=function(i) {
  if (i < 10) {
    i = "0" + i;
  }
  return i + "";
}
var addTime=function(time,time_tab) {
  //计算倒计时时间
  var days = parseInt(time / 1000 / 60 / 60 / 24, 10); //计算剩余的天数
  time_tab.days = addZero(days).split("");
  var hours = parseInt(time / 1000 / 60 / 60 % 24, 10); //计算剩余的小时
  time_tab.hours = addZero(hours).split("");
  var minutes = parseInt(time / 1000 / 60 % 60, 10); //计算剩余的分钟
  time_tab.minutes = addZero(minutes).split("");
  var seconds = parseInt(time / 1000 % 60, 10); //计算剩余的秒数
  time_tab.seconds = addZero(seconds).split("");
}
/**
 * js处理倒计时的主线程插件
 * @params event
 * {
 *  startTime:开始时间毫秒值
 *  endTime:结束时间毫秒值
 *  time_tab:时间倒计时面板需要的参数，初始化格式如下
 *  {
 *    days: [0, 0],
      hours: [0, 0],
      minutes: [0, 0],
      seconds: [0, 0]
 *  }
 * }
 * @return 返回结果格式
 * {
 * time:剩余倒计时毫秒数 Long类型,
 * time_tab: 时间面板 格式如上，object类型,
 * closed:是否需要关闭线程 boolean类型
 * }
 *
 */
addEventListener("message",function (event) {
  if (event.data.startTime < event.data.endTime) {
    var time = Math.abs(event.data.endTime - event.data.startTime);
    var timer = setInterval(function(){
      addTime(time,event.data.time_tab);
      if (time >= 1000) {
        time -= 1000;
        postMessage({time:time,time_tab:event.data.time_tab,closed:false});
      } else {
          clearInterval(timer);
          postMessage({time_tab:{
            days: [0, 0],
            hours: [0, 0],
            minutes: [0, 0],
            seconds: [0, 0]
          },closed:true});
      }
    }, 1000);
  }
},false);
//一下方式也一样
// onmessage = function(event){
//     if (event.data.startTime < event.data.endTime) {
//       var time = Math.abs(event.data.endTime - event.data.startTime);
//       var timer = setInterval(function(){
//         addTime(time,event.data.time_tab);
//         if (time >= 1000) {
//           time -= 1000;
//           postMessage({time:time,time_tab:event.data.time_tab,closed:false});
//         } else {
//           clearInterval(timer);
//           postMessage({time_tab:{
//               days: [0, 0],
//               hours: [0, 0],
//               minutes: [0, 0],
//               seconds: [0, 0]
//             },closed:true});
//         }
//       }, 1000);
//     }
// }




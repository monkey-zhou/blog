import Router from 'vue-router'
import store from '@/vuex/store.js'
import util from '@/utils/pro_util'
import components from '@/router/component'


import {
  Toast,
  Dialog,
  Notify
} from 'vant';

Vue.use(Router);

const router = new Router({
  mode: 'history',
  routes: components
})

//定义一个白名单路由数组

const WHITE_PATH_URL = [];
for (var i = 0; i < components.length; i++) {
  if (components[i].path !== '/home') {
    WHITE_PATH_URL.push(components[i].path);
  }
}
WHITE_PATH_URL.push("/cache/tabs");

//判断数组中是否包含某元素
var isExit= function(str,array){
  var flag=false;
  for(var i=0;i<array.length;i++){
    if(str==array[i]){
      flag=true;
    }
  }
  return flag;
}

console.log("路由白名单为"+WHITE_PATH_URL);

//校验token是否过期 //不需要了
var validTokenExp =function (){
  return util.base64decode(store.getters.getValForKey("token")).exp > (new Date().getTime());
}

//全局路由前守卫
router.beforeEach((to, from, next) => {
  console.log(to);
  console.log(from);
  console.log("登录前的URL"+store.getters.getValForKey("beforeLoginUrl"));
  // NProgress.start();
  if (isExit(to.path,WHITE_PATH_URL)) {
    if(to.path=="/login"){
      if (store.getters.getValForKey("token") != null) {
        // //判断本地记录的登录状态是否已经失效；//不需要了
        // if (validTokenExp()) {
        //   // 说明当前token过期时间到了
        //   console.log("token没过期");
          router.replace("/home");
        //   return;
        // } else {
        //   console.log("token过期了");
        //   store.commit("clearToken");
        // }
      }
    }
    next();
    return;
  }
  //判断是否已经登录过
  if (store.getters.getValForKey("token") != null) {
    // //判断本地记录的登录状态是否已经失效；
    // console.log("111111111111111111111111");
    // console.log(util.base64decode(store.getters.getValForKey("token")).exp);
    // console.log("111111111111111111111111");
    // console.log(util.base64decode(store.getters.getValForKey("token")).exp > (new Date().getTime()));
    // console.log("now",(new Date().getTime()));
    // console.log("exp",util.base64decode(store.getters.getValForKey("token")).exp );
    // if (validTokenExp()) {
    //   console.log("token没过期2");
      next();
    // } else {
    //   Toast.loading({
    //     mask:true,
    //     message: "登录已失效",
    //     duration: 1200
    //   });
    //   store.commit("clearToken");
    //   next("/login");
    // }
  }else{
    // Toast.loading({
    //   mask:true,
    //   message: '请先登录',
    //   duration: 1000
    // });
    store.commit("clearToken");
    next("/login");
    // MessageBox.alert('登录后方可访问', '未登录', {
    //     confirmButtonText: '现在就去',
    //     callback: action => {
    //       console.log(`${ action }`);
    //       if (`${ action }` == 'cancel') {
    //         router.replace("/"); //回到首页
    //         return;
    //       } else {
    //         router.replace("/login");
    //         return;
    //       }
    //     }
    //   }
    // )
  }
})

//全局路由解析守卫
router.beforeResolve((to, from, next) => {
  console.log(to);
  console.log(from);
  if(to.path!='/login'){
    store.commit("saveBeforeLoginUrl",to.fullPath);
  }
  // NProgress.done();
  next();
})
//全局路由后置钩子
router.afterEach((to, from) => {
  console.log(to);
  console.log(from);
  // document.title=to.name;
})

export default router;
